#version 330 core
layout (location = 0) in vec3 aPos;

out vec3 texCoords;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_parentModel;
uniform mat4 u_projection;

void main()
{
    texCoords = aPos;
    vec4 pos = u_projection * u_view * u_parentModel * u_model * vec4(aPos, 1.0);
    gl_Position = pos.xyww;
}