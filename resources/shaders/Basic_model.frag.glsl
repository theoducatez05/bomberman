#version 330

struct	ColorData {
	bool isTexture;

	vec3 color;
	sampler2D texture;
};

struct	Material {
	ColorData diffuse1;
	ColorData specular1;
	ColorData normal1;
	vec3 ambient;

	float shininess;
};

struct Light {
    vec3 position;

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec2 textureCoordinates;
in vec3 FragPos;
in vec3 normal;

uniform Material material;
uniform Light light;

uniform vec3 u_viewPos;

out vec4 FragColor;

void main()
{
	// Retrieve alpha
	float alpha = 1.0f;
	if (material.diffuse1.isTexture) {
		alpha = texture(material.diffuse1.texture, textureCoordinates).a;
	}

	// Skip pixel if too transparent
	if (alpha < 0.01)
		discard;

	// Get the diffuse color
	vec3 diffuse;

	if (material.diffuse1.isTexture) {												// If texture is a texture
		diffuse = vec3(texture(material.diffuse1.texture, textureCoordinates));
	}
	else {																			// If material color
		diffuse = material.diffuse1.color;
	}

	// Get the ambient lighting
	vec3 ambient = light.ambient * material.ambient;

	// Get the diffuse lighting
	vec3 norm = normalize(normal);
	vec3 lightDir = normalize(light.position - FragPos);
	float diff = max(dot(norm, lightDir), 0.0);
	vec3 diffuseLight = diff * light.diffuse;

	// Get the specular lighting
	vec3 specularStrength;
	if (material.specular1.isTexture) {												// If texture is a texture
		specularStrength = vec3(texture(material.specular1.texture, textureCoordinates));
	}
	else {																			// If material color
		specularStrength = material.specular1.color;
	}

	vec3 viewDir = normalize(u_viewPos - FragPos);
	vec3 reflectDir = reflect(-lightDir, norm);
	float spec = pow(max(dot(viewDir, reflectDir), 0.0), 32);
	vec3 specular = (specularStrength * spec) * light.specular;

	// Adding up everything
	vec3 result = diffuse * (ambient + diffuseLight + specular);

	FragColor = vec4(result, alpha);
}