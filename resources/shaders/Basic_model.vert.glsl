#version 330

layout (location = 0) in vec3 a_position;
layout (location = 1) in vec3 a_normals;
layout (location = 2) in vec2 a_textureCoordinates;

uniform mat4 u_model;
uniform mat4 u_view;
uniform mat4 u_projection;
uniform mat4 u_parentModel;

out vec2 textureCoordinates;
out vec3 normal;
out vec3 FragPos;

void main()
{
    // Vertex position
    gl_Position = u_projection * u_view * u_parentModel * u_model * vec4(a_position.x, a_position.y, a_position.z, 1.0);

    //
    FragPos = vec3(u_model * vec4(a_position, 1.0));

    // Texture coordinates
    textureCoordinates = a_textureCoordinates;

    // To do on CPU if possible
    normal = mat3(transpose(inverse(u_model))) * a_normals;
}