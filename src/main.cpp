// General includes
#include <iostream>
#include <vector>
#include <math.h>
#include <iomanip>
#include <sstream>

// GL includes
#include <GL/glew.h>
#include <GL/freeglut.h>

// glm includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Project includes
#include "Program.h"
#include "utils/opengl/camera/Camera.h"
#include "utils/InputManager.h"
#include "utils/opengl/Shader.h"
#include "utils/opengl/GraphicObject.h"
#include "utils/opengl/Skybox.h"
#include "Player.h"
#include "Box.h"
#include "Monster.h"
#include "Bomb.h"
#include "ImageUI.h"
#include "Explosion.h"
#include <utils/opengl/camera/TopDownCamera.h>

// Objects
std::shared_ptr<GraphicObject> floor1;
std::vector<std::shared_ptr<GraphicObject>> walls;
std::shared_ptr<GraphicObject> terrain;
std::shared_ptr<GraphicObject> cube;
std::shared_ptr<GraphicObject> end;
std::unique_ptr<Skybox> skybox;

// Default object Y positions
const float BOMB_HEIGHT = 6.2f;		// 0.0624m		-- > 6.2
const float BOX_HEIGHT = 9.3f;		// 0.093m		-- > 9.3
const float GHOST_HEIGHT = 6.6f;	// 0.066035m	-- > 6.6
const float POWERUP_HEIGHT = 6.6f;	// 0.066035m	-- > 6.6
const float WALLS_HEIGHT = 12.37f;	// 0.123745m	-- > 12.37
const float DOG_HEIGHT = 2.5f;		// 0.025m		-- > 2.5

// For the power up rotations
float sinValue = 0.0f;

/**
 * @brief Glut main diplsay method (used in the main glut loop)
 *
 */
void display()
{

	glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Draw the walls
	for (std::shared_ptr<GraphicObject> wall : walls) {
		wall->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}
	// Draw the boxes
	for (std::shared_ptr<GraphicObject> wall : Program::getInstance().boxes) {
		wall->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}

	// Draw the explosions if they are on
	if (Program::getInstance().explosion->m_drawIt) {
		Program::getInstance().explosion->drawExplosion(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}

	// Draw the other objects in the scene
	Program::getInstance().bomb->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	Program::getInstance().blastPowerUp1->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	Program::getInstance().blastPowerUp2->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	Program::getInstance().speedPowerUp1->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	floor1->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	Program::getInstance().player->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	Program::getInstance().dog->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	terrain->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	cube->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	end->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	// Draw the skybox
	skybox->draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());

	// Draw the UI
	Program::getInstance().livesImage->draw();
	Program::getInstance().startImage->draw();
	Program::getInstance().gameOverImage->draw();
	Program::getInstance().winImage->draw();
	Program::getInstance().tutoImage->draw();

	// Draw the text
	if (Program::getInstance().gameStarted) {
		std::stringstream stream;
		stream << "TIMER : " << std::fixed << std::setprecision(0) << Program::getInstance().timer;
		std::string text = stream.str();
		glRasterPos2f(0.0f, 0.9f);
		glColor3f(1.0f, 0.0f, 0.0f);
		glutBitmapString(GLUT_BITMAP_TIMES_ROMAN_24, (const unsigned char*)text.c_str());
	}

	glutSwapBuffers();
}

/**
 * @brief Objects and shader initialization function
 *
 */
void init()
{
	glEnable(GL_DEPTH_TEST);

	// Set up the light light object
	Program::getInstance().lightShader = std::make_shared<Shader>(Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Light.frag.glsl");
	cube = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Miscellaneous\\Cube\\Cube.obj", Program::getInstance().lightShader, false);
	cube->getTransform().setPosition({ 0.0f, 1500.0f, 800.0f });
	cube->getTransform().setScale({ 10.0f, 10.0f, 10.0f });
	cube->setLit(false);

	// Set up the basic model shader
	Program::getInstance().basicModelShader = std::make_shared<Shader>(Program::getInstance().RESOURCE_FOLDER + "shaders\\Basic_model.vert.glsl",
		Program::getInstance().RESOURCE_FOLDER + "shaders\\Basic_model.frag.glsl");
	Program::getInstance().basicModelShader->bind();
	Program::getInstance().basicModelShader->setUniform3fv("light.ambient", { 0.7f, 0.7f, 0.7f });
	Program::getInstance().basicModelShader->setUniform3fv("light.diffuse",  {0.85f, 0.85f, 0.85f});
	Program::getInstance().basicModelShader->setUniform3fv("light.specular", { 0.5f, 0.5f, 0.5f });
	Program::getInstance().basicModelShader->setUniform3fv("light.position", cube->getTransform().getPosition());
	Program::getInstance().basicModelShader->setUniform3fv("u_viewPos", Program::getInstance().camera->getPosition());

	// Set up the floor and the terrain
	floor1 = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Structures\\Floor\\Floor.fbx", Program::getInstance().basicModelShader, false);
	terrain = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Structures\\Terrain\\Terrain3.fbx", Program::getInstance().basicModelShader, false);

	// Set up the bomb
	Program::getInstance().bomb = std::make_shared<Bomb>(Program::getInstance().RESOURCE_FOLDER + "models\\Items\\Bomb\\Bomb.fbx", Program::getInstance().basicModelShader, false);
	Program::getInstance().bomb->setHidden(true);

	// All objects placed based on the array map
	for (int i = 0; i < Program::getInstance().mapWidth; ++i) {
		for (int j = 0; j < Program::getInstance().mapHeight; ++j) {
			// Walls
			if (Program::getInstance().map[i][j] == '#') {
				walls.push_back(std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\Structures\\Walls\\WallCube.fbx", Program::getInstance().basicModelShader, false));
				walls.back()->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, WALLS_HEIGHT, Program::getInstance().mapCoord[i][j].y });
				walls.back()->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
			}
			// Boxes
			else if (Program::getInstance().map[i][j] == 'b') {
				Program::getInstance().boxes.push_back(std::make_shared<Box>(Program::getInstance().RESOURCE_FOLDER + "models\\Items\\Box\\Box.fbx", Program::getInstance().basicModelShader, false));
				Program::getInstance().boxes.back()->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, BOX_HEIGHT, Program::getInstance().mapCoord[i][j].y });
				Program::getInstance().boxes.back()->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
			}
			// Monster
			else if (Program::getInstance().map[i][j] == 'M') {
				Program::getInstance().dog = std::make_shared<Monster>(Program::getInstance().RESOURCE_FOLDER + "models\\Enemies\\Dog\\Dog.fbx", Program::getInstance().basicModelShader, false);
				Program::getInstance().dog->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, DOG_HEIGHT, Program::getInstance().mapCoord[i][j].y});
				Program::getInstance().dog->getTransform().setRotationY(glm::pi<float>());
				Program::getInstance().dog->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
			}
			// Player
			else if (Program::getInstance().map[i][j] == 'P') {
				Program::getInstance().map[i][j] = ' ';
				Program::getInstance().player = std::make_shared<Player>(Program::getInstance().RESOURCE_FOLDER + "models\\Player\\Player.fbx", Program::getInstance().basicModelShader, false);
				Program::getInstance().player->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, 0.0f, Program::getInstance().mapCoord[i][j].y });
				Program::getInstance().player->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
			}
			// Ending
			else if (Program::getInstance().map[i][j] == 'E'){
				end = std::make_shared<GraphicObject>(Program::getInstance().RESOURCE_FOLDER + "models\\End.fbx", Program::getInstance().lightShader, false);
				end->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, 0.0f, Program::getInstance().mapCoord[i][j].y });
				end->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
				end->setLit(false);
			}
			// Power-up 1
			else if (Program::getInstance().map[i][j] == '1') {
				Program::getInstance().blastPowerUp1 = std::make_shared<Player>(Program::getInstance().RESOURCE_FOLDER + "models\\PowerUp\\Blast.fbx", Program::getInstance().lightShader, false);
				Program::getInstance().blastPowerUp1->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, POWERUP_HEIGHT, Program::getInstance().mapCoord[i][j].y });
				Program::getInstance().blastPowerUp1->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
				Program::getInstance().blastPowerUp1->setLit(false);
			}
			// Power-up 2
			else if (Program::getInstance().map[i][j] == '2') {
				Program::getInstance().blastPowerUp2 = std::make_shared<Player>(Program::getInstance().RESOURCE_FOLDER + "models\\PowerUp\\Blast.fbx", Program::getInstance().lightShader, false);
				Program::getInstance().blastPowerUp2->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, POWERUP_HEIGHT, Program::getInstance().mapCoord[i][j].y });
				Program::getInstance().blastPowerUp2->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
				Program::getInstance().blastPowerUp2->setLit(false);
			}
			// Power - up 3
			else if (Program::getInstance().map[i][j] == '3') {
				Program::getInstance().speedPowerUp1 = std::make_shared<Player>(Program::getInstance().RESOURCE_FOLDER + "models\\PowerUp\\Blast.fbx", Program::getInstance().lightShader, false);
				Program::getInstance().speedPowerUp1->getTransform().setPosition({ Program::getInstance().mapCoord[i][j].x, POWERUP_HEIGHT, Program::getInstance().mapCoord[i][j].y });
				Program::getInstance().speedPowerUp1->getTransform().setLevelGridPos(Program::getInstance().gridPosFromWorldPos({ Program::getInstance().mapCoord[i][j].x, Program::getInstance().mapCoord[i][j].y }));
				Program::getInstance().speedPowerUp1->setLit(false);
			}
		}
	}

	// Set up the small bomb
	Program::getInstance().smallBomb = std::make_shared<Bomb>(Program::getInstance().RESOURCE_FOLDER + "models\\Items\\Bomb\\Bomb.fbx", Program::getInstance().basicModelShader, false);
	Program::getInstance().smallBomb->getTransform().setScale({ 0.15f, 0.15f, 0.15f });
	Program::getInstance().smallBomb->getTransform().setPosition({ 0.0f, 23.0f, 0.0f });
	Program::getInstance().player->addChild(Program::getInstance().smallBomb);

	// Set up the explosion object model
	Program::getInstance().explosion = std::make_shared<Explosion>(Program::getInstance().RESOURCE_FOLDER + "models\\Miscellaneous\\Explosion\\Explosion.fbx", Program::getInstance().lightShader, false);
	Program::getInstance().explosion->getTransform().setPositionY(10.0f);
	Program::getInstance().explosion->getTransform().setScale({ 0.1f, 0.1f, 0.1f });
	Program::getInstance().explosion->setLit(false);

	// Set up the UI Elements
	Program::getInstance().gameOverImage = std::make_shared<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\Game_over.png");
	Program::getInstance().winImage = std::make_shared<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\Win.png");
	Program::getInstance().tutoImage = std::make_shared<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\Tuto.png");
	Program::getInstance().startImage = std::make_shared<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\Start.png");
	Program::getInstance().startImage->m_hidden = false;
	Program::getInstance().livesImage = std::make_shared<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_3.png");

	// Set up the skybox
	skybox = std::make_unique<Skybox>();
}

/**
 * @brief For refreshing the static objects in the scene after the end of the game
 *
 * Used before the reload
 */
void refresh() {
	walls.clear();
	floor1.reset();
	terrain.reset();
	cube.reset();
}

/**
 * @brief Process a window resize event
 *
 * @param width The new window width
 * @param height The new window height
 */
void resizeWindow(int width, int height)
{
	InputManager::getInstance().mousePositionX = width/2.0f;
	InputManager::getInstance().mousePositionY = height/2.0f;

	Program::getInstance().windowHeight = height;
	Program::getInstance().windowWidth = width;

	glViewport(0, 0, width, height); // reset the viewport
	Program::getInstance().projection = glm::perspective(glm::radians(45.0f), (float)Program::getInstance().windowWidth/ Program::getInstance().windowHeight, 0.1f, 5000.0f);
}

/**
 * @brief Glut idle function callback, to update the scene.
 *
 */
void updateScene()
{

	// Update program deltaTime
	Program::getInstance().updateDeltaTime((long)glutGet(GLUT_ELAPSED_TIME));

	// Used when the game just started
	if (Program::getInstance().startPressed == true && Program::getInstance().gameStarted == false) {
		Program::getInstance().startImage->m_hidden = true;
		Program::getInstance().livesImage->m_hidden = false;
		Program::getInstance().gameStarted = true;
		Program::getInstance().player->setHidden(false);
		Program::getInstance().camera = std::make_unique <TopDownCamera>(Program::getInstance().player);
	}

	// When the player wins or loses we reload
	if (Program::getInstance().timer <= 0.0f || Program::getInstance().player->m_lives <= 0 || Program::getInstance().gameWon) {
		glutPostRedisplay();
		Program::refreshInstance();
		refresh();
		init();
	}

	// Update the bomb
	if (Program::getInstance().bomb->m_isOn) {
		Program::getInstance().bomb->m_detonationTimer -= Program::getInstance().deltaTime;
		if (Program::getInstance().bomb->m_detonationTimer <= 0.0f) {
			Program::getInstance().bomb->explode();
			Program::getInstance().player->m_bombs++;
		}
	}

	// Update the player invinsibility after taking damages
	if (Program::getInstance().player->m_isInvincible) {
		Program::getInstance().player->m_invincibilityTime -= Program::getInstance().deltaTime;
		if (Program::getInstance().player->m_invincibilityTime <= 0.0f) {
			Program::getInstance().player->m_invincibilityTime = Program::getInstance().player->m_invincibilityTimeMax;
			Program::getInstance().player->m_isInvincible = false;
		}
	}

	// Update the explosions (scale and if it needs to be drawn)
	if (Program::getInstance().explosion->m_drawIt) {
		Program::getInstance().explosion->getTransform().setScale(Program::getInstance().explosion->getTransform().getScale() + (Program::getInstance().deltaTime * 2.35f));
		Program::getInstance().explosion->m_duration -= Program::getInstance().deltaTime;
		if (Program::getInstance().explosion->m_duration <= 0.0f) {
			Program::getInstance().explosion->m_drawIt = false;
			Program::getInstance().explosion->m_duration = Program::getInstance().explosion->m_durationMax;
			Program::getInstance().explosion->getTransform().setScale({ 0.1f, 0.1f, 0.1f });
		}
	}

	// Make the monster walk
	Program::getInstance().dog->walk();

	// Process keyboard inputs
	InputManager::processKeyInputs();

	// Rotate the power-ups
	Program::getInstance().blastPowerUp1->getTransform().setRotationY(Program::getInstance().blastPowerUp1->getTransform().getRotationY() + Program::getInstance().deltaTime * 2.0f);
	Program::getInstance().blastPowerUp2->getTransform().setRotationY(Program::getInstance().blastPowerUp2->getTransform().getRotationY() + Program::getInstance().deltaTime * 2.0f);
	Program::getInstance().speedPowerUp1->getTransform().setRotationY(Program::getInstance().speedPowerUp1->getTransform().getRotationY() + Program::getInstance().deltaTime * 2.0f);

	Program::getInstance().blastPowerUp1->getTransform().setRotationX(Program::getInstance().blastPowerUp1->getTransform().getRotationX() + Program::getInstance().deltaTime * 2.0f);
	Program::getInstance().blastPowerUp2->getTransform().setRotationX(Program::getInstance().blastPowerUp2->getTransform().getRotationX() + Program::getInstance().deltaTime * 2.0f);
	Program::getInstance().speedPowerUp1->getTransform().setRotationX(Program::getInstance().speedPowerUp1->getTransform().getRotationX() + Program::getInstance().deltaTime * 2.0f);

	// Rotate the small bomb over the player's head
	sinValue += 0.05f;
	if (sinValue >= 100000) {
		sinValue = 0.0f;
	}
	Program::getInstance().smallBomb->getTransform().setPositionX(3.0f * cos(sinValue));
	Program::getInstance().smallBomb->getTransform().setPositionZ(3.0f * sin(sinValue));

	// Draw the next frame
	glutPostRedisplay();
}

/**
 * @brief Main function
 *
 * @param argc System arguments count
 * @param argv System arguments
 */
int main(int argc, char** argv)
{
	// Set up the window
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(Program::getInstance().windowWidth, Program::getInstance().windowHeight);
	glutCreateWindow("CS7GV6 | Individual project: Bomberman | Theo Ducatez");

	GLenum res = glewInit();

	// Check for any errors
	if (res != GLEW_OK) {
		std::cout << "Error: \n" << glewGetErrorString(res) << std::endl;
		return 1;
	}

	// Set up input configurations and callbacks
	glutSetCursor(GLUT_CURSOR_NONE);
	glutKeyboardFunc(InputManager::keyPress);
	glutKeyboardUpFunc(InputManager::keyUp);
	glutPassiveMotionFunc(InputManager::mouseMoveFPS);

	// Register general call back funtions
	glutDisplayFunc(display);
	glutIdleFunc(updateScene);
	glutReshapeFunc(resizeWindow);

	// Setting up the graphic objects
	init();

	// Begin infinite event loop
	glutMainLoop();
	return 0;
}