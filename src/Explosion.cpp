#include "Explosion.h"

#include <string>
#include <memory>

#include "utils/opengl/Shader.h"
#include "Program.h"

/**
* @brief The constructor
*
* \param modelFilePath The model file path
* \param shader The shader object
* \param flip If we need to flip the textures or not
*/
Explosion::Explosion(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) : GraphicObject(modelFilePath, shader, flip) {}

/**
* @brief Draws the explosions
*
* \param projection The projection matrice
* \param view The view matrix
* \param parentModel The parent model if any
*/
void Explosion::drawExplosion(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel) {

	glm::vec2 bombGridPos = Program::getInstance().bomb->getTransform().getLevelGridPos();
	glm::vec2 bombArrayPos = Program::arrayPosFromGridPos({ bombGridPos.x, bombGridPos.y });

	glm::vec2 leftTile = { bombGridPos.x - Program::getInstance().bomb->m_power, bombGridPos.y };
	glm::vec2 toptTile = { bombGridPos.x, bombGridPos.y - Program::getInstance().bomb->m_power };
	glm::vec2 rightTile = { bombGridPos.x + Program::getInstance().bomb->m_power, bombGridPos.y };
	glm::vec2 bottomTile = { bombGridPos.x, bombGridPos.y + Program::getInstance().bomb->m_power };

	for (int i = bombGridPos.x; i >= leftTile.x; --i) {
		glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, leftTile.y });
		if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
			break;
		}
		getTransform().setPositionX(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].x);
		getTransform().setPositionZ(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].y);
		draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}

	for (int i = bombGridPos.x; i <= rightTile.x; ++i) {
		glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, leftTile.y });
		if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
			break;
		}
		getTransform().setPositionX(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].x);
		getTransform().setPositionZ(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].y);
		draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}

	for (int i = bombGridPos.y; i >= toptTile.y; --i) {
		glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({toptTile.x, i });
		if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
			break;
		}
		getTransform().setPositionX(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].x);
		getTransform().setPositionZ(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].y);
		draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}

	for (int i = bombGridPos.y; i <= bottomTile.y; ++i) {
		glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ toptTile.x, i });
		if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
			break;
		}
		getTransform().setPositionX(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].x);
		getTransform().setPositionZ(Program::getInstance().mapCoord[(int)arrayTestPos.x][(int)arrayTestPos.y].y);
		draw(Program::getInstance().projection, Program::getInstance().camera->GetViewMatrix());
	}
}
