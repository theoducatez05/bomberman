#include "Program.h"

Program* Program::instance = nullptr;

/**
* @brief Get the unique instance
*
* @return The program instance
*/
Program& Program::getInstance()
{
	if (!instance)
		instance = new Program;
	return *instance;
}

/**
* @brief Refresh the unique instance
*
* @return The program instance
*/
void Program::refreshInstance()
{
	if (instance)
		delete instance;
		instance = new Program;
}

/**
* @brief Updates the program deltaTime
*
* @param elapsedTime Time between two frames
*/
void Program::updateDeltaTime(long elapsedTime)
{
	if (lastFrame == 0)
		lastFrame = elapsedTime;
	deltaTime = (elapsedTime - lastFrame) * 0.001f;
	lastFrame = elapsedTime;

	if (startPressed) {
		timer -= deltaTime;
	}
}