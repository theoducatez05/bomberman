#pragma once
/*****************************************************************//**
 * @file   Monster.h
 * @brief The Monster header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/
#include "utils/opengl/GraphicObject.h"

/**
 * @brief The Monster class
 */
class Monster :
    public GraphicObject
{
public:
    /**
     * @brief The constructor
     *
     * \param modelFilePath The model file path
     * \param shader The shader object
     * \param flip If we need to flip the textures or not
     */
    Monster(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);

    /**
     * @brief Makes the monster walk
     *
     */
    void walk();

private:

    // The current direction (false is up and true is down)
    bool m_direction = 1;
};

