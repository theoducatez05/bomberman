#pragma once
/*****************************************************************//**
 * @file   Explosion.h
 * @brief The Explosion header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/

#include "utils/opengl/GraphicObject.h"

/**
 * @brief The Explosion class
 */
class Explosion :
    public GraphicObject
{
public:
    /**
     * @brief The constructor
     *
     * \param modelFilePath The model file path
     * \param shader The shader object
     * \param flip If we need to flip the textures or not
     */
    Explosion(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);

    /**
     * @brief Draws the explosions
     *
     * \param projection The projection matrice
     * \param view The view matrix
     * \param parentModel The parent model if any
     */
    void drawExplosion(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel = glm::mat4(1.0f));

    // The duration of the explosion
    float m_durationMax = 0.4f;
    float m_duration = m_durationMax;

    // The moment the explosion started
    float m_explosionTime;

    // If we need to draw the explosion
    bool m_drawIt = false;

    // The original scale of the explosion before scaling
    float m_originalScale = 0.1f;
};

