#include "Box.h"

#include <string>
#include <memory>

#include "utils/opengl/Shader.h"

/**
* @brief The constructor
*
* \param modelFilePath The model file path
* \param shader The shader object
* \param flip If we need to flip the textures or not
*/
Box::Box(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) : GraphicObject(modelFilePath, shader, flip) {}