#pragma once
/*****************************************************************//**
 * @file   Player.h
 * @brief The Player header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/

#include "utils/opengl/GraphicObject.h"

#include "Box.h"

/**
 * @brief The bomb class
 */
class Bomb :
    public GraphicObject
{
public:
    /**
     * @brief The constructor
     *
     * \param modelFilePath The model file path
     * \param shader The shader object
     * \param flip If we need to flip the textures or not
     */
    Bomb(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);

    // If the bomb is on
    bool     m_isOn = false;

    // Detonation informations
    float   m_detonationTimerMax = 3.0f;
    float   m_detonationTimer = m_detonationTimerMax;
    float   m_placementTime;

    // The power
    int     m_power = 1;

    // The boxes to break when it will explode
    std::vector<std::shared_ptr<Box>> m_adjacentBoxes;

    /**
     * @brief To make the bomb explode
     *
     */
    void explode();
};

