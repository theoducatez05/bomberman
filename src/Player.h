#pragma once
/*****************************************************************//**
 * @file   Player.h
 * @brief The Player header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/

#include "utils/opengl/GraphicObject.h"

/**
 * @brief The player class
 */
class Player :
    public GraphicObject
{
public :
    /**
     * @brief The constructor
     *
     * \param modelFilePath The model file path
     * \param shader The shader object
     * \param flip If we need to flip the textures or not
     */
    Player(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);

    /**
     * @brief Makes the player take damages
     *
     * \param value The amount to give
     */
    void takeDamages(int value);

    /**
     * @brief Things to do when the player dies
     *
     */
    void die();

    // The player lives
    int m_lives = 3;
    // The number of bombs the player has
    int m_bombs = 1;

    // If the player is invincible or not
    bool m_isInvincible = false;
    // The max timer
    float m_invincibilityTimeMax = 3.0f;
    // The invicibility current time
    float m_invincibilityTime = m_invincibilityTimeMax;
};

