#include "InputManager.h"

#include <GL/freeglut.h>

#include "glm/gtc/constants.hpp"

#include "utils/opengl/camera/FPSCamera.h"
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/opengl/camera/TopDownCamera.h"

#include <iostream>

/**
* @brief The singleton get instance method
*
* @return The instance
*/
InputManager& InputManager::getInstance()
{
	static InputManager instance;
	return instance;
}

/**
* @brief When a key is released
*
* @param key The key
*/
void InputManager::keyUp(unsigned char key, int x, int y) {

	getInstance().keyPressed[key] = false;

	glutPostRedisplay();
}

/**
 * @brief When a key is pressed but doesn't need real time reaction
 *
 * @param key The key
 */
void InputManager::keyPress(unsigned char key, int x, int y)
{
	getInstance().keyPressed[key] = true;

	if (getInstance().keyPressed[27]) {
		if (getInstance().inMenu) {
			glutSetCursor(GLUT_CURSOR_INHERIT);
			glutPassiveMotionFunc(mouseMove);
			getInstance().inMenu = false;
		}
		else {
			glutSetCursor(GLUT_CURSOR_NONE);
			glutPassiveMotionFunc(mouseMoveFPS);
			getInstance().inMenu = true;
		}
	}
	if (getInstance().keyPressed['c']) {
		CameraType cameraType = Program::getInstance().camera->getType();
		glm::vec3 cameraPreviousPosition = Program::getInstance().camera->getPosition();
		Program::getInstance().camera.release();

		if (cameraType == CameraType::FREE) {
			Program::getInstance().camera = std::make_unique<FPSCamera>(Program::getInstance().player);
		}
		else if (cameraType == CameraType::FPS) {
			Program::getInstance().player->setHidden(false);
			Program::getInstance().camera = std::make_unique <TopDownCamera>(Program::getInstance().player);
		}
		else {
			Program::getInstance().player->setHidden(false);
			Program::getInstance().camera = std::make_unique <FreeCamera>(Program::getInstance().player->getTransform().getPosition() + 5.0f);
		}
	}
	if (getInstance().keyPressed[9]) {
		if (Program::getInstance().tutoImage->m_hidden == true) {
			Program::getInstance().tutoImage->m_hidden = false;
		}
		else {
			Program::getInstance().tutoImage->m_hidden = true;
		}
	}
	// Places a bomb
	if (getInstance().keyPressed[32]) {

		if (!Program::getInstance().startPressed) {
			Program::getInstance().startPressed = true;
		}
		else {
			if (Program::getInstance().player->m_bombs > 0) {

				// Determine position
				glm::vec2 bombGridPos;
				float playerRotation = Program::getInstance().player->getTransform().getRotationY();
				glm::vec2 playerGridPos = Program::getInstance().player->getTransform().getLevelGridPos();
				if (playerRotation > -glm::half_pi<float>() / 2 && playerRotation < glm::half_pi<float>() / 2) {
					// Bottom
					bombGridPos = { playerGridPos.x, playerGridPos.y + 1 };
				}
				else if (playerRotation < -glm::half_pi<float>() / 2 && playerRotation > -1.5f * glm::half_pi<float>()) {
					// left
					bombGridPos = { playerGridPos.x - 1 , playerGridPos.y };
				}
				else if (playerRotation < -1.5f * glm::half_pi<float>() || playerRotation > 1.5f * glm::half_pi<float>()) {
					// top
					bombGridPos = { playerGridPos.x, playerGridPos.y - 1 };
				}
				else {
					// right
					bombGridPos = { playerGridPos.x + 1, playerGridPos.y };
				}

				glm::vec2 bombArrayPos = Program::arrayPosFromGridPos(bombGridPos);
				if (Program::getInstance().map[(int)bombArrayPos.x][(int)bombArrayPos.y] == ' ') {
					Program::getInstance().map[(int)bombArrayPos.x][(int)bombArrayPos.y] = 'x';
					Program::getInstance().player->m_bombs--;
					Program::getInstance().smallBomb->setHidden(true);
					Program::getInstance().bomb->setHidden(false);
					Program::getInstance().bomb->m_isOn = true;
					Program::getInstance().bomb->m_placementTime = glutGet(GLUT_ELAPSED_TIME);

					Program::getInstance().bomb->getTransform().setLevelGridPos(bombGridPos);
					Program::getInstance().bomb->getTransform().setPosition({ Program::getInstance().mapCoord[(int)bombArrayPos.x][(int)bombArrayPos.y].x,
						Program::getInstance().BOMB_HEIGHT,
						Program::getInstance().mapCoord[(int)bombArrayPos.x][(int)bombArrayPos.y].y });

					// Get the adjacent boxes and not the ones behind walls
					bool add = true;
					glm::vec2 leftTile = { bombGridPos.x - Program::getInstance().bomb->m_power, bombGridPos.y };
					glm::vec2 toptTile = { bombGridPos.x, bombGridPos.y - Program::getInstance().bomb->m_power };
					glm::vec2 rightTile = { bombGridPos.x + Program::getInstance().bomb->m_power, bombGridPos.y };
					glm::vec2 bottomTile = { bombGridPos.x, bombGridPos.y + Program::getInstance().bomb->m_power };
					for (std::shared_ptr<Box> box : Program::getInstance().boxes) {
						glm::vec2 boxGridPos = box->getTransform().getLevelGridPos();
						if (boxGridPos.y == leftTile.y && boxGridPos.x >= leftTile.x && boxGridPos.x <= rightTile.x) {
							if (boxGridPos.x >= leftTile.x && boxGridPos.x <= bombGridPos.x) {

								for (int i = boxGridPos.x; i <= bombGridPos.x; ++i) {
									glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, leftTile.y });
									if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
										add = false;
										break;
									}
								}

								if (add)
									Program::getInstance().bomb->m_adjacentBoxes.push_back(box);
							}
							add = true;
							if (boxGridPos.x <= rightTile.x && boxGridPos.x >= bombGridPos.x) {

								for (int i = boxGridPos.x; i >= bombGridPos.x; --i) {
									glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, rightTile.y });
									if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
										add = false;
										break;
									}
								}

								if (add)
									Program::getInstance().bomb->m_adjacentBoxes.push_back(box);
							}
						}
						else if (boxGridPos.x == toptTile.x && boxGridPos.y >= toptTile.y && boxGridPos.y <= bottomTile.y) {
							if (boxGridPos.y >= toptTile.y && boxGridPos.y <= bombGridPos.y) {

								for (int i = boxGridPos.y; i <= bombGridPos.y; ++i) {
									glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ toptTile.x, i });
									if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
										add = false;
										break;
									}
								}

								if (add)
									Program::getInstance().bomb->m_adjacentBoxes.push_back(box);
							}
							add = true;
							if (boxGridPos.y <= bottomTile.y && boxGridPos.y >= bombGridPos.y) {

								for (int i = boxGridPos.y; i >= bombGridPos.y; --i) {
									glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ bottomTile.x, i });
									if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
										add = false;
										break;
									}
								}
								if (add)
									Program::getInstance().bomb->m_adjacentBoxes.push_back(box);
							}
						}
					}
				}
			}
		}
	}

	glutPostRedisplay();
}

/**
 * @brief Process user keyboard inputs.
 *
 */
void InputManager::processKeyInputs()
{
	if (Program::getInstance().gameStarted) {
		// Camera movements
		if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['q'])	// Diagonal up left
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_LEFT, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['z'] && InputManager::getInstance().keyPressed['d'])	// Diagonal up right
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD_RIGHT, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['q'])	// Diagonal down left
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_LEFT, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['s'] && InputManager::getInstance().keyPressed['d'])	// Diagonal down right
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD_RIGHT, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['z'])
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::FORWARD, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['s'])
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::BACKWARD, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['q'])
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::LEFT, Program::getInstance().deltaTime);
		else if (InputManager::getInstance().keyPressed['d'])
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::RIGHT, Program::getInstance().deltaTime);
		else
			Program::getInstance().camera->ProcessKeyboard(CameraMovement::NONE, Program::getInstance().deltaTime);
	}
}

/**
 * @brief Mouse movement callback on FPS mode
 *
 * @param x X position
 * @param y Y position
 */
void InputManager::mouseMoveFPS(int x, int y)
{
	float xoffset = x - getInstance().mousePositionX;
	float yoffset = getInstance().mousePositionY - y; // reversed since y-coordinates range from bottom to top

	// For rotation when the mouse is close to an edge
	float detectionLimitY = Program::getInstance().windowHeight * 0.2f;
	float detectionLimitX = Program::getInstance().windowWidth * 0.2f;

	getInstance().mousePositionX = static_cast<float>(x);
	getInstance().mousePositionY = static_cast<float>(y);

	// Process the movement with the camera
	Program::getInstance().camera->ProcessMouseMovement(xoffset, yoffset, Program::getInstance().deltaTime);

	glutPostRedisplay();


	// Placing the mouse back to the middle of the screen if at the edges
	float centerX = Program::getInstance().windowHeight / 2.0f;
	float centerY = Program::getInstance().windowHeight / 2.0f;

	if (x < detectionLimitX || x > Program::getInstance().windowWidth - detectionLimitX) {  //you can use values other than 100 for the screen edges if you like, kind of seems to depend on your mouse sensitivity for what ends up working best
		getInstance().mousePositionX = centerX;   //centers the last known position, this way there isn't an odd jump with your cam as it resets
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);  //centers the cursor
	}
	else if (y < detectionLimitY || y > Program::getInstance().windowHeight - detectionLimitY) {
		getInstance().mousePositionX = centerX;
		getInstance().mousePositionY = centerY;
		glutWarpPointer((int)centerX, (int)centerY);
	}
}