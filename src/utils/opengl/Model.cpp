#include "Model.h"

// General imports
#include <string>
#include <iostream>
#include <memory>

// ASSIMP imports
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// Program imports
#include "Mesh.h"
#include "Texture.h"
#include "Material.h"

/**
* @brief Constructor with file
*
* \param filePath THe model file path
* \param flipTextures If we need to flip the textures
*/
Model::Model(const std::string& filePath, bool flipTextures)
{
    m_flipTextures = flipTextures;
    loadModel(filePath);
}

/**
 * @brief Constructor with meshes and directory
 *
 * \param meshes The meshes
 * \param directory The directory
 * \param flipTextures If we need to flip the textures
 */
Model::Model(std::vector<std::shared_ptr<Mesh>>& meshes, std::string& textDirectory, bool flipTextures) :
    m_meshes(meshes),
    m_directory(textDirectory),
    m_flipTextures(flipTextures) {}

/**
 * @brief Draws the mesh with its textures (embeded or not)
 *
 * @param shader The shader
 * @param lit If the mesh has to take into account the light or not
 */
void Model::draw(const std::shared_ptr <Shader> shader, bool lit)
{
    for (int i = 0; i < m_meshes.size(); ++i) {
        m_meshes[i]->draw(shader, lit);
    }
}

/**
 * @brief Loads the model from a file path
 *
 * @param filePath The model (FBX or OBJ file path)
 */
void Model::loadModel(const std::string& filePath)
{
    Assimp::Importer import;
    const aiScene* scene = import.ReadFile(filePath,
        aiProcess_Triangulate |
        aiProcess_FlipUVs |
        aiProcess_PreTransformVertices |
        aiProcess_GenNormals |
        aiProcess_GenUVCoords |
        aiProcess_LimitBoneWeights |
        aiProcess_CalcTangentSpace);
    //aiProcess_GlobalScale

    if (!scene || scene->mFlags & AI_SCENE_FLAGS_INCOMPLETE || !scene->mRootNode)
    {
        std::cout << "ERROR::ASSIMP::LOAD_MODEL::" << import.GetErrorString() << std::endl;
        return;
    }
    m_directory = filePath.substr(0, filePath.find_last_of('\\'));

    processNode(scene->mRootNode, scene);
}

/**
 * @brief Processes a node from ASSIMP importer API
 *
 * @param node The ASSIMP node
 * @param scene The ASSIMP scene
 */
void Model::processNode(aiNode* node, const aiScene* scene)
{
    // Process all the node's meshes (if any)
    for (unsigned int i = 0; i < node->mNumMeshes; i++)
    {
        aiMesh* mesh = scene->mMeshes[node->mMeshes[i]];
        m_meshes.push_back(processMesh(mesh, scene));
    }
    // Then do the same for each of its children
    for (unsigned int i = 0; i < node->mNumChildren; i++)
    {
        processNode(node->mChildren[i], scene);
    }
}

/**
 * @brief Processes a ASSIMP mesh
 *
 * @param mesh The ASSIMP mesh
 * @param scene The ASSIMP sccene
 * @return A pointer to a mesh
 */
std::shared_ptr<Mesh> Model::processMesh(aiMesh* mesh, const aiScene* scene)
{
    std::vector<Vertex> vertices;
    std::vector<unsigned int> indices;
    std::vector<std::shared_ptr<Texture>> textures;
    Material material = Material();

    for (unsigned int i = 0; i < mesh->mNumVertices; i++)
    {
        Vertex vertex;

        // Process vertex positions
        glm::vec3 vector;
        vector.x = mesh->mVertices[i].x;
        vector.y = mesh->mVertices[i].y;
        vector.z = mesh->mVertices[i].z;
        vertex.position = vector;

        // Process normals
        vector.x = mesh->mNormals[i].x;
        vector.y = mesh->mNormals[i].y;
        vector.z = mesh->mNormals[i].z;
        vertex.normal = vector;

        // Process textures coordinates
        if (mesh->mTextureCoords[0]) // Assimp allow for more than 1 texture coordinate per vertex (up to 8)
        {
            glm::vec2 vec;
            vec.x = mesh->mTextureCoords[0][i].x;
            vec.y = mesh->mTextureCoords[0][i].y;
            vertex.textCoordinates = vec;
        }
        else
            vertex.textCoordinates = glm::vec2(0.0f, 0.0f);

        vertices.push_back(vertex);
    }

    // Process the indices
    for (unsigned int i = 0; i < mesh->mNumFaces; i++)
    {
        aiFace face = mesh->mFaces[i];
        for (unsigned int j = 0; j < face.mNumIndices; j++)
            indices.push_back(face.mIndices[j]);
    }

    // Process the material
    if (mesh->mMaterialIndex >= 0)
    {
        aiMaterial* aiMaterial = scene->mMaterials[mesh->mMaterialIndex];

        // Process textures
        std::vector<std::shared_ptr<Texture>> diffuseMaps = loadMaterialTextures(scene, aiMaterial, aiTextureType_DIFFUSE, TextureType::Diffuse);
        textures.insert(textures.end(), diffuseMaps.begin(), diffuseMaps.end());

        std::vector<std::shared_ptr<Texture>> specularMaps = loadMaterialTextures(scene, aiMaterial, aiTextureType_SPECULAR, TextureType::Specular);
        textures.insert(textures.end(), specularMaps.begin(), specularMaps.end());

        // Process material
        material = loadMaterial(aiMaterial);

    }

    return std::make_shared<Mesh>(vertices, indices, textures, material);
}

/**
 * @brief Loads the textures and materials
 *
 * @param scene The ASSIMP scene
 * @param mat The ASSIMP material
 * @param type The texture type
 * @param type texture type
 * @return A pointer to a texture
 */
std::vector<std::shared_ptr<Texture>> Model::loadMaterialTextures(const aiScene* scene, aiMaterial* mat, aiTextureType type, TextureType textureType)
{
    std::vector<std::shared_ptr<Texture>> textures;
    for (unsigned int i = 0; i < mat->GetTextureCount(type); i++)
    {
        aiString str;
        mat->GetTexture(type, i, &str);
        bool skip = false;
        for (unsigned int j = 0; j < m_texturesLoaded.size(); j++)
        {
            if (m_texturesLoaded[j]->getFilePath() == str.C_Str())
            {
                textures.push_back(m_texturesLoaded[j]);
                skip = true;
                break;
            }
        }
        if (!skip)
        {   // if texture hasn't been loaded already, load it
            std::string filePath = std::string(str.C_Str());

            std::shared_ptr<Texture> texture;
            // If its an embedded texture type
            if (auto embeded_texture = scene->GetEmbeddedTexture(str.C_Str())) {
                texture = std::make_shared<Texture>(embeded_texture, filePath.c_str(), textureType, m_flipTextures);
            }
            // Or a egular file texture type
            else {
                filePath = m_directory + '/' + filePath;
                texture = std::make_shared<Texture>(filePath.c_str(), textureType, m_flipTextures);
            }

            textures.push_back(texture);
            m_texturesLoaded.push_back(texture); // add to loaded textures
        }
    }
    return textures;
}

/**
 * @brief Loads a material
 *
 * @param aiMat The ASSIMP material
 * @return A material object
 */
Material Model::loadMaterial(aiMaterial* aiMat)
{
    Material	material = Material();
    aiColor3D	color(0.f, 0.f, 0.f);
    float		shininess;
    aiString	matName;

    // Name
    if (aiMat->Get(AI_MATKEY_NAME, matName) == AI_SUCCESS) {
        material.setName(matName.C_Str());
    }

    // Diffuse
    if (aiMat->Get(AI_MATKEY_COLOR_DIFFUSE, color) == AI_SUCCESS) {
        material.setDiffuse(glm::vec3(color.r, color.g, color.b));
    }

    // Ambient
    if (aiMat->Get(AI_MATKEY_COLOR_AMBIENT, color) == AI_SUCCESS) {
        //material.setAmbient(glm::vec3(color.r, color.g, color.b));
        material.setAmbient(glm::vec3(0.4f, 0.4f, 0.4f));
    }

    // Specular
    if (aiMat->Get(AI_MATKEY_COLOR_SPECULAR, color) == AI_SUCCESS) {
        material.setSpecular(glm::vec3(color.r, color.g, color.b));
    }

    // Shininess
    if (aiMat->Get(AI_MATKEY_SHININESS, shininess) == AI_SUCCESS) {
        material.setShininess(shininess);
    }

    return material;
}

