#include "GraphicObject.h"

// General includes
#include <memory>
#include <string>

// Program includes
#include "Model.h"
#include "Shader.h"

GraphicObject::GraphicObject() :
	m_model(nullptr),
	m_shader(nullptr),
	m_transform(Transform()) {}

/**
 * @brief Constructor with model and shader
 *
 * \param model The model
 * \param shader The shaders
 */
GraphicObject::GraphicObject(std::shared_ptr<Model> model, std::shared_ptr<Shader> shader):
	m_model(model),
	m_shader(shader),
	m_transform(Transform()) {}

/**
 * @brief Constructor with the model file path and the shader
 *
 * \param modelFilePath The model file
 * \param shader The shaders
 * \param flip If we need to flip the textures
 */
GraphicObject::GraphicObject(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) :
	m_model(std::make_shared<Model>(modelFilePath, flip)),
	m_shader(shader),
	m_transform(Transform()) {}


GraphicObject& GraphicObject::operator=(GraphicObject& graphicObject)
{
	if (&graphicObject != this) {
		m_model = graphicObject.getModel();
		m_shader = graphicObject.getShader();
		m_transform = graphicObject.getTransform();
	}

	return *this;
}

GraphicObject::~GraphicObject() {}

/**
 * @brief Adds a child to this object
 *
 * \param graphicObject THe chil object
 */
void GraphicObject::addChild(std::shared_ptr<GraphicObject> graphicObject)
{
	m_children.push_back(graphicObject);
}

/**
* @brief Draw the object
*
* @param projection The projection matrix
* @param view The view matrix
* @param parentModel The parent model matrix (if any)
*/
void GraphicObject::draw(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel)
{
	// If object is hidden, don't draw it
	if (m_hidden)
		return;

	glm::mat4 mvp;

	m_shader->bind();

	if (parentModel == glm::mat4(1.0f)) {								// If object doesn't have a parent, draw it normally
		m_shader->setUniformMatrix4fv("u_projection", projection);
		m_shader->setUniformMatrix4fv("u_view", view);
	}
	else {																// Else use only parent model
		m_shader->setUniformMatrix4fv("u_projection", glm::mat4(1.0f));
		m_shader->setUniformMatrix4fv("u_view", glm::mat4(1.0f));
	}

	m_shader->setUniformMatrix4fv("u_parentModel", parentModel);
	m_shader->setUniformMatrix4fv("u_model", m_transform.getModelTransform());

	m_model->draw(m_shader, m_lit);

	// Drawing children objects
	mvp = projection * view * parentModel * m_transform.getModelTransform();

	for (std::shared_ptr<GraphicObject> child : m_children) {
		child->draw(projection, view, mvp);
	}
}