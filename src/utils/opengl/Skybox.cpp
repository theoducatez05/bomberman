#include "Skybox.h"

// General imports
#include <iostream>

// GL imports
#include <GL/glew.h>

// STB_image import
#include "utils/stb_image.h"

// The vertices of the cubemap
const float Skybox::_vertices[] = {
	-1.0f,  1.0f, -1.0f,
	-1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f, -1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,

	-1.0f, -1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f, -1.0f,  1.0f,
	-1.0f, -1.0f,  1.0f,

	-1.0f,  1.0f, -1.0f,
	1.0f,  1.0f, -1.0f,
	1.0f,  1.0f,  1.0f,
	1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f,  1.0f,
	-1.0f,  1.0f, -1.0f,

	-1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f, -1.0f,
	1.0f, -1.0f, -1.0f,
	-1.0f, -1.0f,  1.0f,
	1.0f, -1.0f,  1.0f
};

Skybox::Skybox():
	m_shader(std::make_shared<Shader>(SHADER_SKYBOX_VS, SHADER_SKYBOX_FS)),
	m_vertexBuffer(std::make_shared<VertexBuffer>(sizeof(_vertices) / sizeof(_vertices[0]), &_vertices[0], 3 * sizeof(float))),
	m_vertexArray(std::make_shared<VertexArray>())
{
	VertexBufferLayout vertexBufferLayout = VertexBufferLayout();
	vertexBufferLayout.pushf(3, 0);

	m_vertexArray->link(m_vertexBuffer, nullptr, vertexBufferLayout);

	std::vector<std::string> skyboxFaces = {
		std::string(SKYBOX_PATH) + SKYBOX_NAME_RIGHT + SKYBOX_EXT,  // right
		std::string(SKYBOX_PATH) + SKYBOX_NAME_LEFT + SKYBOX_EXT,  // left
		std::string(SKYBOX_PATH) + SKYBOX_NAME_TOP + SKYBOX_EXT,  // up
		std::string(SKYBOX_PATH) + SKYBOX_NAME_BOTTOM + SKYBOX_EXT,  // down
		std::string(SKYBOX_PATH) + SKYBOX_NAME_FRONT + SKYBOX_EXT,  // front
		std::string(SKYBOX_PATH) + SKYBOX_NAME_BACK + SKYBOX_EXT,  // back
	};
	load(skyboxFaces);

	m_shader->bind();
	m_shader->setUniform1i("skybox", 0);
}

Skybox::~Skybox(){}

/**
 * @brief Loads the skybox object and texture
 *
 * @param faces The different texture faces paths
 */
void Skybox::load(std::vector<std::string>& skyboxFaces)
{
	glGenTextures(1, &m_textureID);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureID);

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

	int width;
	int height;
	int nrChannels;
	for (unsigned int i = 0; i < skyboxFaces.size(); ++i) {
		unsigned char* data = stbi_load(skyboxFaces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data) {
			if ((width & (width - 1)) != 0 || (height & (height - 1)) != 0) {
				std::cout << "image " << skyboxFaces[i] << " is not power of 2 dimensions" << std::endl;
			}
			GLenum format = GL_RGB;
			if (nrChannels == 1) {
				format = GL_RED;
			}
			else if (nrChannels == 3) {
				format = GL_RGB;
			}
			else if (nrChannels == 4) {
				format = GL_RGBA;
			}
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
				0, static_cast<GLint>(format), width, height, 0, format, GL_UNSIGNED_BYTE, data);
			stbi_image_free(data);
		}
		else {
			std::cout << "Skybox texture failed to load at path: " << skyboxFaces[i] << std::endl;
			stbi_image_free(data);
		}
	}

	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

/**
 * @brief Draws the Skybox
 *
 * @param projection The projection matrix
 * @param view The view matrix
 */
void Skybox::draw(glm::mat4 projection, glm::mat4 view)
{
	glDepthFunc(GL_LEQUAL);  // Change depth function so depth test passes when values are equal to depth buffer's content
	m_shader->bind();
	m_shader->setUniformMatrix4fv("u_view", glm::mat4(glm::mat3(view)));
	m_shader->setUniformMatrix4fv("u_projection", projection);
	m_shader->setUniformMatrix4fv("u_model", glm::mat4(1.0f));
	m_shader->setUniformMatrix4fv("u_parentModel", glm::mat4(1.0f));
	m_vertexArray->bind();
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_CUBE_MAP, m_textureID);
	glDrawArrays(GL_TRIANGLES, 0, 36);
	m_vertexArray->unbind();
	glDepthFunc(GL_LESS); // Set depth function back to default
}