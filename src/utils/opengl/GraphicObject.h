#pragma once
/*****************************************************************//**
 * @file   GraphicObject.h
 * @brief  The general graphic object class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <vector>
#include <string>
#include <memory>
#include <vector>

// GL includes
#include <glm/glm.hpp>

// Program includes
#include "Model.h"
#include "Transform.h"

/**
 * @brief The general graphic object class
 */
class GraphicObject
{
private:
	// Model and shader
	std::shared_ptr<Model>	m_model;
	std::shared_ptr<Shader>	m_shader;

	// Position, rotation, scale object transform
	Transform				m_transform;

	// General attribues
	bool					m_lit = true;
	bool					m_hidden = false;

	// Object children
	std::vector<std::shared_ptr<GraphicObject>> m_children;

public:

	/**
	 * @brief Constructor with model and shader
	 *
	 * \param model The model
	 * \param shader The shaders
	 */
	GraphicObject(std::shared_ptr<Model> model, std::shared_ptr<Shader> shader);

	/**
	 * @brief Constructor with the model file path and the shader
	 *
	 * \param modelFilePath The model file
	 * \param shader The shaders
	 * \param flip If we need to flip the textures
	 */
	GraphicObject(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);
	GraphicObject();
	~GraphicObject();

	GraphicObject& operator=(GraphicObject& graphicObject);

	/**
	 * @brief Draw the object
	 *
	 * @param projection The projection matrix
	 * @param view The view matrix
	 * @param parentModel The parent model matrix (if any)
	 */
	void draw(glm::mat4 projection, glm::mat4 view, glm::mat4 parentModel = glm::mat4(1.0f));

	/**
	 * @brief Adds a child to this object
	 *
	 * \param graphicObject THe chil object
	 */
	void addChild(std::shared_ptr<GraphicObject> graphicObject);

	// Getters
	inline bool isHidden() { return m_hidden; }
	inline bool isLit() { return m_lit; }
	inline void setHidden(bool hidden) { m_hidden = hidden; }
	inline void setLit(bool lit) { m_lit = lit; }

	inline std::shared_ptr<Model> getModel() { return m_model; };
	inline std::shared_ptr<Shader> getShader() { return m_shader; };
	inline Transform& getTransform() { return m_transform; }

	inline std::vector<std::shared_ptr<GraphicObject>>& getChildren() { return m_children; };
};