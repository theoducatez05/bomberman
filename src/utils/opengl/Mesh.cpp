#include "Mesh.h"

// General includes
#include <memory>
#include <iostream>

// GL includes
#include <GL/glew.h>

/**
* @brief Constructor
*
* \param vertices The vertices
* \param indices The indices
* \param textures The textures
* \param material THe material
*/
Mesh::Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<std::shared_ptr<Texture>> textures, Material material) :
	m_vertices(vertices),
	m_indices(indices),
	m_textures(textures),
	m_material(material),
    m_vertexBuffer(std::make_shared<VertexBuffer>(m_vertices.size(), &m_vertices[0], sizeof(Vertex))),
    m_indexBuffer(std::make_shared<IndexBuffer>(m_indices)),
    m_vertexArray(std::make_shared<VertexArray>())
{
	setupMesh();
}

/**
* @brief Sets up the mesh buffers
*
*/
void Mesh::setupMesh()
{
    VertexBufferLayout vertexBufferLayout = VertexBufferLayout();
    vertexBufferLayout.pushvec3f(1, 0);
    vertexBufferLayout.pushvec3f(1, 1);
    vertexBufferLayout.pushvec2f(1, 2);

    m_vertexArray->link(m_vertexBuffer, m_indexBuffer, vertexBufferLayout);
}

/**
 * @brief Draw the mesh
 *
 * @param shader The shader
 * @param lit If the mesh is supposed to be lit
 */
void Mesh::draw(const std::shared_ptr <Shader> shader, bool lit)
{
	// Textures count
	unsigned int diffuseNr = 1;
	unsigned int specularNr = 1;
	unsigned int normalNr = 1;

	// Lighting flags
	bool hasDiffuseTex = false;
	bool hasSpecularTex = false;
	bool hasNormalTex = false;;

	shader->bind();

	for (unsigned int i = 0; i < m_textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // Activate proper texture unit before binding

		std::string number;
		TextureType textureType = m_textures[i]->getType();
		std::string textureTypeString;

		if (textureType == TextureType::Diffuse) {
			number = std::to_string(diffuseNr++);
			textureTypeString = "diffuse";
			hasDiffuseTex = true;
		}
		else if (textureType == TextureType::Specular) {
			continue; // Temporary
			number = std::to_string(specularNr++);
			textureTypeString = "specular";
			hasSpecularTex = true;
		}
		else if (textureType == TextureType::Normal) {
			continue; // Temporary
			number = std::to_string(specularNr++);
			textureTypeString = "normal";
			hasNormalTex = true;
		}

		shader->setUniform1i(("material." + textureTypeString + number + ".texture").c_str(), i);
		shader->setUniform1b(("material." + textureTypeString + number + ".isTexture").c_str(), true);
		glBindTexture(GL_TEXTURE_2D, m_textures[i]->getId());
	}
	glActiveTexture(GL_TEXTURE0);

	// Putting the isTexture and colors in case there are no textures
	if (!hasDiffuseTex) {
		for (unsigned int i = 1; i <= diffuseNr; i++) {
			shader->setUniform1b(("material.diffuse" + std::to_string(i) + ".isTexture").c_str(), false);
			shader->setUniform3fv(("material.diffuse" + std::to_string(i) + ".color").c_str(), m_material.getDiffuse());
		}
	}

	if (lit) {
		if (!hasSpecularTex) {
			for (unsigned int i = 1; i <= specularNr; i++) {
				shader->setUniform1b(("material.specular" + std::to_string(i) + ".isTexture").c_str(), false);
				shader->setUniform3fv(("material.specular" + std::to_string(i) + ".color").c_str(), m_material.getSpecular());
			}
		}
		// Not yet used
		/*if (!hasNormalTex) {
			for (unsigned int i = 1; i <= normalNr; i++) {
				shader->setUniform1b(("material.normal" + std::to_string(i) + ".isTexture").c_str(), false);
				shader->setUniform3fv(("material.normal" + std::to_string(i) + ".color").c_str(), glm::vec3(0, 0, 1));
			}
		}*/

		//shader->setUniform1f("material.shininess", mesh->getMaterial().getShininess());
		shader->setUniform3fv("material.ambient", m_material.getAmbient());
	}

	// Binding buffers
	m_vertexArray->bind();
	m_indexBuffer->bind();

	glDrawElements(GL_TRIANGLES, static_cast<GLsizei>(m_indexBuffer->getCount()), GL_UNSIGNED_INT, nullptr);

	// Unbinding the textures
	for (unsigned int i = 0; i < m_textures.size(); i++)
	{
		glActiveTexture(GL_TEXTURE0 + i); // activate proper texture unit before binding
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glActiveTexture(GL_TEXTURE0);
}

