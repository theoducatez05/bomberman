#include "Material.h"

// General imports
#include <string>

// GLM imports
#include <glm/glm.hpp>

/**
* @brief Constructor
*
* \param name The name
* \param diffuse Diffuse value
* \param specular Specular value
* \param ambient Ambient value
* \param shininess Shininess value
*/
Material::Material(std::string const name, glm::vec3 const diffuse,
	glm::vec3 const specular, glm::vec3 const ambient, float const shininess)
	: m_name(name),
	m_diffuse(diffuse),
	m_specular(specular),
	m_ambient(ambient),
	m_shininess(shininess) {
}

Material::Material(Material const& src) {
	*this = src;
}

Material::~Material() {}

Material& Material::operator=(Material const& rhs) {
	if (this != &rhs) {
		m_name = rhs.getName();
		m_diffuse = rhs.getDiffuse();
		m_specular = rhs.getSpecular();
		m_ambient = rhs.getAmbient();
		m_shininess = rhs.getShininess();
	}
	return *this;
}