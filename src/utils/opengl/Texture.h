#pragma once
/*****************************************************************//**
 * @file   Texture.h
 * @brief  The Texture and TextureType header ile
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <string>

// ASSIMP includes
#include <assimp/scene.h>

/**
 * @brief Lists the different texture types available
 */
enum class TextureType {Diffuse, Specular, Normal};

/**
 * @brief Represents a texture OPENGL
 */
class Texture
{
private:
	unsigned int	m_id;
	TextureType		m_type;
	std::string		m_filePath;

public:
	/**
	 * @brief Texture from a file
	 *
	 * @param m_filePath The file path
	 * @param type The texture type
	 * @param flip If needed to flip the texture
	 */
	Texture(const char* m_filePath, const TextureType type, bool flip);
	/**
	 * @brief Texture from embeded data (FBX)
	 *
	 * @param texture The embeded texture data
	 * @param name The texture name
	 * @param type The texture type
	 * @param flip IF needed to flip the texture
	 */
	Texture(const aiTexture* texture, const char* name, TextureType type, bool flip);
	~Texture();

	// Getters and setters
	inline unsigned int getId() {return m_id;}
	inline const TextureType getType() { return m_type; }
	inline const std::string& getFilePath() { return m_filePath; }

private:
	/**
	 * @brief Creates the texture
	 *
	 * @param textureData The texture data bytes
	 * @param type The texture type
	 * @param width The width
	 * @param height The Height
	 * @param nrChannels The number of channels
	 * @param flip If needed to flip the texture
	 */
	void createTexture(unsigned char* textureData, const TextureType type, int width, int height, int nrChannels, bool flip);
};

