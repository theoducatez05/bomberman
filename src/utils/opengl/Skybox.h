#pragma once
/*****************************************************************//**
 * @file   Skybox.h
 * @brief  The SkyBox header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General inputs
#include <vector>
#include <memory>

// GLM inputs
#include <glm/glm.hpp>

// Program imports
#include "utils/opengl/Shader.h"
#include "utils/opengl/VertexArray.h"
#include "utils/opengl/VertexBuffer.h"
#include "Program.h"

// Skybox shaders paths
#define SHADER_SKYBOX_VS Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.vert.glsl"
#define SHADER_SKYBOX_FS Program::getInstance().RESOURCE_FOLDER + "shaders\\Skybox.frag.glsl"

// Textures paths and extension
#define SKYBOX_PATH	Program::getInstance().RESOURCE_FOLDER + "textures\\skybox\\Mountains\\"
#define SKYBOX_EXT		".jpg"

// Textures names
#define SKYBOX_NAME_RIGHT	"right"
#define SKYBOX_NAME_LEFT	"left"
#define SKYBOX_NAME_TOP		"top"
#define SKYBOX_NAME_BOTTOM	"bottom"
#define SKYBOX_NAME_FRONT	"front"
#define SKYBOX_NAME_BACK	"back"

/**
 * @brief Represents a Skybox
 */
class Skybox
{
private:
	std::shared_ptr<Shader>				m_shader;
	unsigned int						m_textureID;

	std::shared_ptr<VertexArray>		m_vertexArray;
	std::shared_ptr<VertexBuffer>		m_vertexBuffer;

	// The vertices of the CubeMap object
	static const float					 _vertices[];

public:
	Skybox();
	virtual ~Skybox();

	/**
	 * @brief Loads the skybox object and texture
	 *
	 * @param faces The different texture faces paths
	 */
	void load(std::vector<std::string>& faces);
	/**
	 * @brief Draws the Skybox
	 *
	 * @param projection The projection matrix
	 * @param view The view matrix
	 */
	void draw(glm::mat4 projection, glm::mat4 view);

	// Getters and setters
	inline std::shared_ptr<Shader> getShader() { return m_shader; }
	inline std::shared_ptr<Shader> const& getShader() const { return m_shader; }
	inline unsigned int getTextureID() const { return m_textureID; }
};