#pragma once
/*****************************************************************//**
 * @file   TopDownCamera.h
 * @brief  The TopDownCamera class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

#include <memory>

#include <glm/glm.hpp>

#include "Camera.h"
#include "utils/opengl/GraphicObject.h"
#include "Player.h"

// Default camera values
const float     TOPDOWN_CAMERA_DEFAULT_YAW = 90.0f;
const float     TOPDOWN_CAMERA_DEFAULT_PITCH = -120.0f;
const glm::vec3 TOPDOWN_CAMERA_DEFAULT_OFFSET = glm::vec3(0.0f, 170.0f, 100.0f);
const float     TOPDOWN_CAMERA_DEFAULT_SPEED = 50.0f;
const float     TOPDOWN_CAMERA_DEFAULT_SENSITIVITY = 15.0f;

/**
 * @brief The top-down camera class
 */
class TopDownCamera : public Camera
{
private:

    // Follow player
    std::shared_ptr<Player> m_playerCharacter;
    glm::vec3 m_offest;

    // Smoothing
    CameraMovement m_previousMovement = CameraMovement::NONE;
    float m_calls = 0.5f;
    float m_maxCalls = 1.0f;

public:

    /**
     * @brief Constructor
     *
     * \param initialPosition Initial position of the camera
     * \param up The UP vector
     * \param yaw The initial yaw
     * \param pitch The initial Pitch
     */
    TopDownCamera(std::shared_ptr<Player> playerCharacter, glm::vec3 offset = TOPDOWN_CAMERA_DEFAULT_OFFSET, glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = TOPDOWN_CAMERA_DEFAULT_YAW, float pitch = TOPDOWN_CAMERA_DEFAULT_PITCH);

     /**
     * @brief Processes input received from any keyboard-like input system.
     *
     * \param direction The direction
     * \param deltaTime The current deltatime
     */
    void ProcessKeyboard(CameraMovement direction, float deltaTime) override;

    /**
     * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
     *
     * \param xoffset The x offset
     * \param yoffset The y offset
     * \param deltaTime The current deltatime
     * \param constrainPitch If we need to constrain the Pitch upward and downward
     */
    void ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch = true) override;
};