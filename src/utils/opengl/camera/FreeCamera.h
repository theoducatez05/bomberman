#pragma once
/*****************************************************************//**
 * @file   FreeCamera.h
 * @brief  The FREECamera class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

#include <glm/glm.hpp>

#include "Camera.h"

// Default camera values
const float FREE_CAMERA_DEFAULT_YAW = -90.0f;
const float FREE_CAMERA_DEFAULT_PITCH = -0.0f;
const float FREE_CAMERA_DEFAULT_SPEED = 100.0f;
const float FREE_CAMERA_DEFAULT_SENSITIVITY = 15.0f;

/**
 * @brief The free camera class
 */
class FreeCamera : public Camera
{
public:

    /**
     * @brief Constructor
     *
     * \param initialPosition Initial position of the camera
     * \param up The UP vector
     * \param yaw The initial yaw
     * \param pitch The initial Pitch
     */
    FreeCamera(glm::vec3 position = glm::vec3(0.0f, 15.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = FREE_CAMERA_DEFAULT_YAW, float pitch = FREE_CAMERA_DEFAULT_PITCH);

    /**
     * @brief Processes input received from any keyboard-like input system.
     *
     * \param direction The direction
     * \param deltaTime The current deltatime
     */
    void ProcessKeyboard(CameraMovement direction, float deltaTime) override;

    /**
     * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
     *
     * \param xoffset The x offset
     * \param yoffset The y offset
     * \param deltaTime The current deltatime
     * \param constrainPitch If we need to constrain the Pitch upward and downward
     */
    void ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch = true) override;
};