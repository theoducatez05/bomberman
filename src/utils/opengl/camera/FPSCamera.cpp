#include "FPSCamera.h"

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Program.h"

/**
 * @brief Constructor
 *
 * \param initialPosition Initial position of the camera
 * \param up The UP vector
 * \param yaw The initial yaw
 * \param pitch The initial Pitch
 */
FPSCamera::FPSCamera(glm::vec3 initialPosition, glm::vec3 up, float yaw, float pitch) :
    Camera(initialPosition, up, yaw, pitch),
    m_playerCharacter(nullptr)
{
    m_type = CameraType::FPS;
    m_mouseSensitivity = FPS_CAMERA_DEFAULT_SENSITIVITY;
    m_movementSpeed = FPS_CAMERA_DEFAULT_SPEED;
    updateCameraVectors();
    updateCameraPositionVectors();
}

/**
 * @brief Constructor with the character
 *
 * \param playerCharacter The main character (Player)
 * \param up The UP vector
 * \param yaw The Yaw
 * \param pitch The Pitch
 */
FPSCamera::FPSCamera(std::shared_ptr<Player> playerCharacter, glm::vec3 up, float yaw, float pitch) :
    Camera(playerCharacter->getTransform().getPosition() + glm::vec3(0.0f, 18.0f, 0.0f),
        up,
        -glm::degrees(playerCharacter->getTransform().getRotationY()) + FPS_CAMERA_DEFAULT_YAW,
        pitch)
{
    m_type = CameraType::FPS;
    m_playerCharacter = playerCharacter;
    m_mouseSensitivity = FPS_CAMERA_DEFAULT_SENSITIVITY;
    m_movementSpeed = FPS_CAMERA_DEFAULT_SPEED;

    playerCharacter->setHidden(true);

    updateCameraVectors();
    updateCameraPositionVectors();
}

/**
 * @brief Processes input received from any keyboard-like input system.
 *
 * \param direction The direction
 * \param deltaTime The current deltatime
 */
void FPSCamera::ProcessKeyboard(CameraMovement direction, float deltaTime)
{

    glm::vec3 positionSave = m_position;

    // Update camera position
    float velocity = m_movementSpeed * deltaTime;
    if (direction == CameraMovement::FORWARD_LEFT) {
        m_position += glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
        m_position -= glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;
    }
    else if (direction == CameraMovement::FORWARD_RIGHT){
        m_position += glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
        m_position += glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;
    }
    else if (direction == CameraMovement::BACKWARD_LEFT){
        m_position -= glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
        m_position -= glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;
    }
    else if (direction == CameraMovement::BACKWARD_RIGHT){
        m_position -= glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
        m_position += glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;
    }
    else if (direction == CameraMovement::FORWARD)
        m_position += glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
    else if (direction == CameraMovement::BACKWARD)
        m_position -= glm::vec3(m_movementFront.x, 0, m_movementFront.z) * velocity;
    else if (direction == CameraMovement::LEFT)
        m_position -= glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;
    else if (direction == CameraMovement::RIGHT)
        m_position += glm::vec3(m_movementRight.x, 0, m_movementRight.z) * velocity;

    updateCameraVectors();
    updateCameraPositionVectors();

    glm::vec2 arrayPos = Program::arrayPosFromGridPos(Program::gridPosFromWorldPos({ m_position.x, m_position.z }));

    //check if player steped on a powerup
    if (Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] == '1') {
        Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().blastPowerUp1->setHidden(true);
    }
    if (Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] == '2') {
        Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().blastPowerUp2->setHidden(true);
    }
    if (Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] == '3') {
        Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().speedPowerUp1->setHidden(true);
    }

    // If player finishes the game
    if (Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] == 'E') {
        Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] = ' ';
        Program::getInstance().gameWon = true;
        Program::getInstance().winImage->m_hidden = false;
    }

    // Check if player is allowed to be there
    if (Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] != 'P' &&
        Program::getInstance().map[(int)arrayPos.x][(int)arrayPos.y] != ' ') {
        m_position = positionSave;
    }

    // Update playerCharacter position
    if (m_playerCharacter) {
        m_playerCharacter->getTransform().setPosition({ m_position.x, m_playerCharacter->getTransform().getPositionY(), m_position.z });
        m_playerCharacter->getTransform().setLevelGridPos(Program::gridPosFromWorldPos({ m_playerCharacter->getTransform().getPositionX(), m_playerCharacter->getTransform().getPositionZ()}));
    }
}

/**
 * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
 *
 * \param xoffset The x offset
 * \param yoffset The y offset
 * \param deltaTime The current deltatime
 * \param constrainPitch If we need to constrain the Pitch upward and downward
 */
void FPSCamera::ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch)
{
    xoffset *= m_mouseSensitivity * deltaTime;
    yoffset *= m_mouseSensitivity * deltaTime;

    m_yaw += xoffset;
    m_pitch += yoffset;

    // make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (m_pitch > 89.0f)
            m_pitch = 89.0f;
        if (m_pitch < -89.0f)
            m_pitch = -89.0f;
    }

    // update Front, Right and Up Vectors using the updated Euler angles
    updateCameraVectors();

    // Update playerCharacter rotation
    if (m_playerCharacter) {
        m_playerCharacter->getTransform().setRotationY(m_playerCharacter->getTransform().getRotationY() - glm::radians(xoffset));
        m_playerCharacter->getTransform().setLevelGridPos(Program::gridPosFromWorldPos({m_playerCharacter->getTransform().getPositionX(), m_playerCharacter->getTransform().getPositionZ()}));
    }
}

/**
 * @brief Custom vectors for the FPS camera orientation
 *
 */
void FPSCamera::updateCameraPositionVectors() {
    // calculate the new Front vector
    glm::vec3 movementFront;
    movementFront.x = cos(glm::radians(m_yaw)) * cos(glm::radians(FPS_CAMERA_DEFAULT_PITCH));
    movementFront.y = sin(glm::radians(m_pitch));
    movementFront.z = sin(glm::radians(m_yaw)) * cos(glm::radians(FPS_CAMERA_DEFAULT_PITCH));
    m_movementFront = glm::normalize(movementFront);
    m_movementRight = glm::normalize(glm::cross(m_movementFront, m_worldUp)); // normalize the vectors, not to slow down the movement
}
