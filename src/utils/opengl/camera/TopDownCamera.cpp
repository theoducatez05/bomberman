#include "TopDownCamera.h"

#include <iostream>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "Program.h"

/**
 * @brief Constructor
 *
 * \param initialPosition Initial position of the camera
 * \param up The UP vector
 * \param yaw The initial yaw
 * \param pitch The initial Pitch
 */
TopDownCamera::TopDownCamera(std::shared_ptr<Player> playerCharacter, glm::vec3 offset, glm::vec3 up, float yaw, float pitch) :
    Camera(playerCharacter->getTransform().getPosition() + offset, up, yaw, pitch)
{
    m_type = CameraType::TOP_DOWN;
    m_playerCharacter = playerCharacter;
    m_offest = offset;
    m_mouseSensitivity = TOPDOWN_CAMERA_DEFAULT_SENSITIVITY;
    m_movementSpeed = TOPDOWN_CAMERA_DEFAULT_SPEED;
    updateCameraVectors();
}

/**
* @brief Processes input received from any keyboard-like input system.
*
* \param direction The direction
* \param deltaTime The current deltatime
*/
void TopDownCamera::ProcessKeyboard(CameraMovement direction, float deltaTime)
{
    float positionVelocity = m_movementSpeed * deltaTime;

    // Smoothing
    if (m_previousMovement == direction) {
        if (m_calls < m_maxCalls)
            m_calls += 0.01f;
    }
    else {
        m_calls = 0.5f;
    }

    glm::vec3 positionSave = m_playerCharacter->getTransform().getPosition();
    glm::vec3 rotationSave = m_playerCharacter->getTransform().getRotation();

    // Move player
    if (direction == CameraMovement::FORWARD_LEFT) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() - positionVelocity);
        m_position.z += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() - positionVelocity);
        m_position.x += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(-135.0f));
    }
    else if (direction == CameraMovement::FORWARD_RIGHT) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() - positionVelocity);
        m_position.z += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() + positionVelocity);
        m_position.x += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(135.0f));
    }
    else if (direction == CameraMovement::BACKWARD_LEFT) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() + positionVelocity);
        m_position.z += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() - positionVelocity);
        m_position.x += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(-45.0f));
    }
    else if (direction == CameraMovement::BACKWARD_RIGHT) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() + positionVelocity);
        m_position.z += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() + positionVelocity);
        m_position.x += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(45.0f));
    }
    else if (direction == CameraMovement::FORWARD) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() - positionVelocity);
        m_position.z += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(180.0f));
    }
    else if (direction == CameraMovement::BACKWARD) {
        m_playerCharacter->getTransform().setPositionZ(m_playerCharacter->getTransform().getPositionZ() + positionVelocity);
        m_position.z += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(0.0f));
    }
    else if (direction == CameraMovement::LEFT) {
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() - positionVelocity);
        m_position.x += - positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(-90.0f));
    }
    else if (direction == CameraMovement::RIGHT) {
        m_playerCharacter->getTransform().setPositionX(m_playerCharacter->getTransform().getPositionX() + positionVelocity);
        m_position.x += + positionVelocity * m_calls;
        m_playerCharacter->getTransform().setRotationY(glm::radians(90.0f));
    }

    glm::vec2 playerArrayPos = Program::arrayPosFromGridPos(Program::gridPosFromWorldPos({ m_playerCharacter->getTransform().getPositionX(), m_playerCharacter->getTransform().getPositionZ() }));

    //check if player steped on a powerup
    if (Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] == '1') {
        Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().blastPowerUp1->setHidden(true);
    }
    if (Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] == '2') {
        Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().blastPowerUp2->setHidden(true);
    }
    if (Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] == '3') {
        Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] = ' ';
        Program::getInstance().bomb->m_power++;
        Program::getInstance().speedPowerUp1->setHidden(true);
    }

    // If player finishes the game
    if (Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] == 'E') {
        Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] = ' ';
        Program::getInstance().gameWon = true;
        Program::getInstance().gameWon = true;
        Program::getInstance().winImage->m_hidden = false;
    }

    // Check if player is allowed to be there
    if (Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] != 'P' &&
        Program::getInstance().map[(int)playerArrayPos.x][(int)playerArrayPos.y] != ' ') {
        m_playerCharacter->getTransform().setPosition(positionSave);
        m_playerCharacter->getTransform().setRotation(rotationSave);
        direction = CameraMovement::NONE;
    }

    m_playerCharacter->getTransform().setLevelGridPos(Program::gridPosFromWorldPos({ m_playerCharacter->getTransform().getPositionX(), m_playerCharacter->getTransform().getPositionZ() }));


    if (direction == CameraMovement::NONE) {

        // Difference entre position camera et position character
        float diffX = 0.0f;
        float diffZ = 0.0f;

        if (m_position.x != m_playerCharacter->getTransform().getPosition().x + m_offest.x) {
            diffX = m_position.x - m_playerCharacter->getTransform().getPosition().x - m_offest.x;
        }

        if (m_position.z != m_playerCharacter->getTransform().getPosition().z + m_offest.z) {
            diffZ = m_position.z - m_playerCharacter->getTransform().getPosition().z - m_offest.z;
        }

        m_position.x -= diffX * 0.01f;
        m_position.z -= diffZ * 0.01f;
    }
    else {
        m_previousMovement = direction;
    }

    updateCameraVectors();
}

/**
 * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
 *
 * \param xoffset The x offset
 * \param yoffset The y offset
 * \param deltaTime The current deltatime
 * \param constrainPitch If we need to constrain the Pitch upward and downward
 */void TopDownCamera::ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch){}