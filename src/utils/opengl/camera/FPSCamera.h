#pragma once
/*****************************************************************//**
 * @file   FPSCamera.h
 * @brief  The FPSCamera class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

#include <memory>

#include <glm/glm.hpp>

#include "Camera.h"
#include "Player.h"

// Default camera values
const float FPS_CAMERA_DEFAULT_YAW = 90.0f;
const float FPS_CAMERA_DEFAULT_PITCH = 0.0f;
const float FPS_CAMERA_DEFAULT_SPEED = 50.0f;
const float FPS_CAMERA_DEFAULT_SENSITIVITY = 15.0f;

/**
 * @brief The FPS camera class
 */
class FPSCamera : public Camera
{
private:

    glm::vec3               m_movementFront;
    glm::vec3               m_movementRight;

    std::shared_ptr<Player> m_playerCharacter;

public:

    /**
     * @brief Constructor
     *
     * \param initialPosition Initial position of the camera
     * \param up The UP vector
     * \param yaw The initial yaw
     * \param pitch The initial Pitch
     */
    FPSCamera(glm::vec3 initialPosition = glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = FPS_CAMERA_DEFAULT_YAW, float pitch = FPS_CAMERA_DEFAULT_PITCH);

    /**
     * @brief Constructor with the character
     *
     * \param playerCharacter The main character (Player)
     * \param up The UP vector
     * \param yaw The Yaw
     * \param pitch The Pitch
     */
    FPSCamera(std::shared_ptr<Player> playerCharacter, glm::vec3 up = glm::vec3(0.0f, 1.0f, 0.0f), float yaw = FPS_CAMERA_DEFAULT_YAW, float pitch = FPS_CAMERA_DEFAULT_PITCH);

    /**
     * @brief Processes input received from any keyboard-like input system.
     *
     * \param direction The direction
     * \param deltaTime The current deltatime
     */
    void ProcessKeyboard(CameraMovement direction, float deltaTime) override;

    /**
     * @brief Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
     *
     * \param xoffset The x offset
     * \param yoffset The y offset
     * \param deltaTime The current deltatime
     * \param constrainPitch If we need to constrain the Pitch upward and downward
     */
    void ProcessMouseMovement(float xoffset, float yoffset, float deltaTime, bool constrainPitch = true) override;

    /**
     * @brief Custom vectors for the FPS camera orientation
     *
     */
    void updateCameraPositionVectors();
};