#pragma once
/*****************************************************************//**
 * @file   Model.h
 * @brief  Class representation of a 3D model, also used to load models fro files
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General imports
#include <vector>
#include <string>
#include <memory>

// ASSIMP imports
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

// Program imports
#include "Mesh.h"
#include "Texture.h"
#include "Shader.h"

/**
 * @brief Represents a 3D Model (set of meshes and textures)
 */
class Model
{
private:
	std::vector<std::shared_ptr<Mesh>>		m_meshes;
	std::string								m_directory;

	// Storing the textures already loaded
	std::vector<std::shared_ptr<Texture>>	m_texturesLoaded;
	bool									m_flipTextures;

public:
	/**
	 * @brief Constructor with file
	 *
	 * \param filePath THe model file path
	 * \param flipTextures If we need to flip the textures
	 */
	Model(const std::string& filePath, bool flipTextures);

	/**
	 * @brief Constructor with meshes and directory
	 *
	 * \param meshes The meshes
	 * \param directory The directory
	 * \param flipTextures If we need to flip the textures
	 */
	Model(std::vector<std::shared_ptr<Mesh>>& meshes, std::string& directory, bool flipTextures);

	/**
	 * @brief Draws the mesh with its textures (embeded or not)
	 *
	 * @param shader The shader
	 * @param lit If the mesh has to take into account the light or not
	 */
	void draw(const std::shared_ptr <Shader> shader, bool lit);

	inline const std::vector<std::shared_ptr<Mesh>>& getMeshes() const { return m_meshes; }
	inline const std::string& getDirectory() const { return m_directory; }

private:
	/**
	 * @brief Loads the model from a file path
	 *
	 * @param filePath The model (FBX or OBJ file path)
	 */
	void loadModel(const std::string& filePath);
	/**
	 * @brief Processes a node from ASSIMP importer API
	 *
	 * @param node The ASSIMP node
	 * @param scene The ASSIMP scene
	 */
	void processNode(aiNode* node, const aiScene* scene);
	/**
	 * @brief Processes a ASSIMP mesh
	 *
	 * @param mesh The ASSIMP mesh
	 * @param scene The ASSIMP sccene
	 * @return A pointer to a mesh
	 */
	std::shared_ptr<Mesh> processMesh(aiMesh* mesh, const aiScene* scene);
	/**
	 * @brief Loads the textures and materials
	 *
	 * @param scene The ASSIMP scene
	 * @param mat The ASSIMP material
	 * @param type The texture type
	 * @param type texture type
	 * @return A pointer to a texture
	 */
	std::vector<std::shared_ptr<Texture>> loadMaterialTextures(const aiScene* scene, aiMaterial* mat, aiTextureType type, TextureType textureType);
	/**
	 * @brief Loads a material
	 *
	 * @param aiMat The ASSIMP material
	 * @return A material object
	 */
	Material loadMaterial(aiMaterial* aiMat);
};

