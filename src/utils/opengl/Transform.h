#pragma once
/*****************************************************************//**
 * @file   Transform.h
 * @brief  The Transform header file
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

//GLM includes
#include <glm/glm.hpp>

// Default transform values
const glm::vec3 defaultPosition = { 0.0f, 0.0f, 0.0f };
const glm::vec3 defaultRotation = { 0.0f, 0.0f, 0.0f };
const glm::vec3 defaultScale = { 1.0f, 1.0f, 1.0f };
const glm::mat4 defaultModelTransform = glm::mat4(1.0f);

/**
 * @brief Represents an Object transform matrix (position, rotation and scale)
 */
class Transform
{
private:
	glm::vec3 m_position;
	glm::vec3 m_rotation;
	glm::vec3 m_scale;
	glm::mat4 m_modelTransform;

	// Position on the level grid
	glm::vec2 m_levelGridPos;

	/**
	 * @brief Updates the model matrix with the current attributes
	 *
	 */
	void updateModelTransform();

public:
	/**
	 * @brief Constructor
	 *
	 * \param position The position
	 * \param orientation The orientation
	 * \param scale The scale
	 */
	Transform(glm::vec3 position = defaultPosition,
		glm::vec3 orientation = defaultRotation,
		glm::vec3 scale = defaultScale);

	Transform& operator=(Transform& transform);


	// Getters and setters
	inline glm::vec3& getPosition() { return m_position; };
	inline glm::vec3& getRotation() { return m_rotation; };
	inline glm::vec3& getScale() { return m_scale; };
	inline glm::mat4 getModelTransform() { return m_modelTransform; }
	inline glm::vec2 getLevelGridPos() { return m_levelGridPos; }

	inline void setPosition(const glm::vec3 position) { m_position = position; updateModelTransform(); };
	inline void setRotation(const glm::vec3 orientation) { m_rotation = orientation; updateModelTransform(); };
	inline void setScale(const glm::vec3 size) { m_scale = size; updateModelTransform(); };
	inline void setLevelGridPos(const glm::vec2 pos) { m_levelGridPos = pos; };

	inline float getPositionX() { return m_position.x; };
	inline float getPositionY() { return m_position.y; };
	inline float getPositionZ() { return m_position.z; };
	inline float getRotationX() { return m_rotation.x; };
	inline float getRotationY() { return m_rotation.y; };
	inline float getRotationZ() { return m_rotation.z; };
	inline float getScaleX() { return m_scale.x; };
	inline float getScaleY() { return m_scale.y; };
	inline float getScaleZ() { return m_scale.z; };

	inline void setPositionX(float x) { m_position.x = x; updateModelTransform(); };
	inline void setPositionY(float y) { m_position.y = y; updateModelTransform(); };
	inline void setPositionZ(float z) { m_position.z = z; updateModelTransform(); };
	inline void setRotationX(float x) { m_rotation.x = x; updateModelTransform(); };
	inline void setRotationY(float y) { m_rotation.y = y; updateModelTransform(); };
	inline void setRotationZ(float z) { m_rotation.z = z; updateModelTransform(); };
	inline void setScaleX(float x) { m_scale.x = x; updateModelTransform(); };
	inline void setScaleY(float y) { m_scale.y = y; updateModelTransform(); };
	inline void setScaleZ(float z) { m_scale.z = z; updateModelTransform(); };
};