#include "ImageUI.h"

#include <iostream>
#include <memory>

#include "Program.h"
#include "GL/glew.h"
#include "utils/stb_image.h"

ImageUI::ImageUI(std::string texturePath) :
    m_texturePath(texturePath)
{
    setup();

    glGenTextures(1, &texture);
    glBindTexture(GL_TEXTURE_2D, texture);
    // set the texture wrapping parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);	// set texture wrapping to GL_REPEAT (default wrapping method)
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    // set texture filtering parameters
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    stbi_set_flip_vertically_on_load(true);

    // load image, create texture and generate mipmaps
    int width, height, nrChannels;
    unsigned char* data = stbi_load(texturePath.c_str(), &width, &height, &nrChannels, 0);
    if (data)
    {
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);
    }
    else
    {
        std::cout << "Failed to load texture" << std::endl;
    }
    stbi_image_free(data);
    stbi_set_flip_vertically_on_load(false);
}

/**
* @brief Sets up the mesh buffers
*
*/
void ImageUI::setup()
{
    if (hasEnding(m_texturePath, "Game_over.png") || hasEnding(m_texturePath, "Win.png") || hasEnding(m_texturePath, "Start.png")) {
        m_vertices.push_back({ {0.65f,  0.65f, 0.0f}, {0.5f,  0.5f, 0.0f}, {1.0f, 1.0f} });
        m_vertices.push_back({ {0.65f, -0.65f, 0.0f}, {0.5f, -0.5f, 0.0f}, {1.0f, 0.0f} });
        m_vertices.push_back({ {-0.65f, -0.65f, 0.0f}, {-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f} });
        m_vertices.push_back({ {-0.65f,  0.65f, 0.0f}, {-0.5f,  0.5f, 0.0f}, {0.0f, 1.0f} });
    }
    else if (hasEnding(m_texturePath, "Tuto.png")) {
        m_vertices.push_back({ {-0.10f, 0.95f, 0.0f}, {-0.5f, -0.5f, 0.0f}, {1.0f, 1.0f}});
        m_vertices.push_back({ {-0.10f,  -0.6f, 0.0f}, {-0.5f,  0.5f, 0.0f}, {1.0f, 0.0f} });
        m_vertices.push_back({ {-1.0f,  -0.6f, 0.0f}, {0.5f,  0.5f, 0.0f},  {0.0f, 0.0f} });
        m_vertices.push_back({ {-1.0f, 0.95f, 0.0f}, {0.5f, -0.5f, 0.0f},  {0.0f, 1.0f} });
    }
    else {
        m_vertices.push_back({ {0.95f,  0.95f, 0.0f}, {0.5f,  0.5f, 0.0f}, {1.0f, 1.0f} });
        m_vertices.push_back({ {0.95f, 0.6f, 0.0f}, {0.5f, -0.5f, 0.0f}, {1.0f, 0.0f} });
        m_vertices.push_back({ {0.6f, 0.6f, 0.0f}, {-0.5f, -0.5f, 0.0f}, {0.0f, 0.0f} });
        m_vertices.push_back({ {0.6f,  0.95f, 0.0f}, {-0.5f,  0.5f, 0.0f}, {0.0f, 1.0f} });
    }

    m_indices.push_back(0);
    m_indices.push_back(1);
    m_indices.push_back(3);
    m_indices.push_back(1);
    m_indices.push_back(2);
    m_indices.push_back(3);

    m_vertexBuffer = std::make_shared<VertexBuffer>(m_vertices.size(), &m_vertices[0], sizeof(Vertex));
    m_indexBuffer = std::make_shared<IndexBuffer>(m_indices);
    m_vertexArray = std::make_shared<VertexArray>();


    VertexBufferLayout vertexBufferLayout = VertexBufferLayout();
    vertexBufferLayout.pushvec3f(1, 0);
    vertexBufferLayout.pushvec3f(1, 1);
    vertexBufferLayout.pushvec2f(1, 2);

    m_vertexArray->link(m_vertexBuffer, m_indexBuffer, vertexBufferLayout);

    m_shader = std::make_shared<Shader>(Program::getInstance().RESOURCE_FOLDER + "shaders\\ImageUI.vert.glsl",
        Program::getInstance().RESOURCE_FOLDER + "shaders\\ImageUI.frag.glsl");
}

void ImageUI::draw() {
    if (!m_hidden) {
        glBindTexture(GL_TEXTURE_2D, texture);

        m_shader->bind();
        m_vertexArray->bind();
        glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
        m_shader->unbind();
        m_vertexArray->unbind();
    }
}

bool ImageUI::hasEnding(std::string const& fullString, std::string const& ending) {
    if (fullString.length() >= ending.length()) {
        return (0 == fullString.compare(fullString.length() - ending.length(), ending.length(), ending));
    }
    else {
        return false;
    }
}