#include "Player.h"

#include <string>
#include <memory>

#include "utils/opengl/Shader.h"
#include "Program.h"

/**
 * @brief The constructor
 *
 * \param modelFilePath The model file path
 * \param shader The shader object
 * \param flip If we need to flip the textures or not
 */
Player::Player(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) : GraphicObject(modelFilePath, shader, flip) {}

/**
 * @brief Makes the player take damages
 *
 * \param value The amount to give
 */
void Player::takeDamages(int value){
	if (!m_isInvincible && m_lives > 0) {
		m_lives--;
	}

	if (m_lives <= 0) {
		die();
	}
}

/**
 * @brief Things to do when the player dies
 *
 */
void Player::die() {
	Program::getInstance().gameOverImage->m_hidden = false;
}
