#include "Monster.h"

#include <string>
#include <memory>

#include "utils/opengl/Shader.h"
#include "Program.h"

/**
 * @brief The constructor
 *
 * \param modelFilePath The model file path
 * \param shader The shader object
 * \param flip If we need to flip the textures or not
 */
Monster::Monster(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) : GraphicObject(modelFilePath, shader, flip) {}

/**
 * @brief Makes the monster walk
 *
 */
void Monster::walk(){
	glm::vec2 gridPosSave = getTransform().getLevelGridPos();
	glm::vec2 arrayPosSave = Program::arrayPosFromGridPos(gridPosSave);
	glm::vec3 positionSave = getTransform().getPosition();

	if (m_direction == false) {
		getTransform().setPositionZ(getTransform().getPositionZ() - Program::getInstance().deltaTime * 150.0f);
	}
	else {
		getTransform().setPositionZ(getTransform().getPositionZ() + Program::getInstance().deltaTime * 150.0f);
	}

	getTransform().setLevelGridPos(Program::gridPosFromWorldPos({getTransform().getPositionX(), getTransform().getPositionZ() }));

	glm::vec2 newArrayPos = Program::getInstance().arrayPosFromGridPos(getTransform().getLevelGridPos());
	if (Program::getInstance().map[(int)newArrayPos.x][(int)newArrayPos.y] != ' ' && Program::getInstance().map[(int)newArrayPos.x][(int)newArrayPos.y] != 'M') {
		getTransform().setPosition(positionSave);
		getTransform().setLevelGridPos(gridPosSave);
		newArrayPos = Program::getInstance().arrayPosFromGridPos(getTransform().getLevelGridPos());

		m_direction = !m_direction;
		getTransform().setRotationY(getTransform().getRotationY() + glm::pi<float>());
	}

	Program::getInstance().map[(int)arrayPosSave.x][(int)arrayPosSave.y] = ' ';
	Program::getInstance().map[(int)newArrayPos.x][(int)newArrayPos.y] = 'M';

	glm::vec2 playerArrayPos = Program::arrayPosFromGridPos(Program::getInstance().player->getTransform().getLevelGridPos());
	if (playerArrayPos == newArrayPos) {
		if (!Program::getInstance().player->m_isInvincible) {
			Program::getInstance().player->takeDamages(1);
			Program::getInstance().player->m_isInvincible = true;

			if (Program::getInstance().player->m_lives == 2) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_2.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 1) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_1.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 0) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_0.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
		}
	}
}
