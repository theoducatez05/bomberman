#pragma once
/*****************************************************************//**
 * @file   ImageUI.h
 * @brief The ImageUI header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/

#include <string>
#include <vector>
#include <memory>

#include "utils/opengl/VertexArray.h"
#include "utils/opengl/VertexBuffer.h"
#include "utils/opengl/VertexBufferLayout.h"
#include "utils/opengl/IndexBuffer.h"
#include "utils/opengl/Mesh.h"

/**
 * .The ImageUI class represents a basic UI
 */
class ImageUI
{
private:
    // mesh data
    std::vector<Vertex>                     m_vertices;
    std::vector<unsigned int>               m_indices;

    //  render data
    std::shared_ptr<VertexBuffer>           m_vertexBuffer;
    std::shared_ptr<VertexArray>            m_vertexArray;
    std::shared_ptr<IndexBuffer>            m_indexBuffer;

    std::shared_ptr<Shader>	                m_shader;

	std::string                             m_texturePath;
    unsigned int texture;

public:
	ImageUI(std::string texturePath);

    // If the mesh needs to be hidden
    bool m_hidden = true;

	void setup();
    void draw();

    /**
     * @brief Allows us to know which vertices to use for each textures
     *
     * \param fullString The full string
     * \param ending The ending we want to know this string has
     * \return If the full string ends with this ending
     */
    bool hasEnding(std::string const& fullString, std::string const& ending);
};

