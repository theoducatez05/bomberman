#pragma once
/*****************************************************************//**
 * @file   Program.h
 * @brief  General singleton program class
 *
 * @author theod
 * @date   November 2021
 *********************************************************************/

// General includes
#include <memory>

// GL includes
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

// Program includes
#include "utils/opengl/camera/FreeCamera.h"
#include "utils/opengl/GraphicObject.h"

#include "Player.h"
#include "Box.h"
#include "Monster.h"
#include "Bomb.h"
#include "ImageUI.h"
#include "Explosion.h"


/**
 * @brief The program singelton class
 */
class Program
{
private:

    /**
     * @brief The program singleton initialization
     *
     * It initializes the coordinate map
     */
    Program()
    {
        for (int i = 0; i < mapWidth; ++i) {
            for (int j = 0; j < mapHeight; ++j) {
                mapCoord[i][j] = { (float)(i - ceil(mapWidth / 2)) * blockWidth, (float)(j - ceil(mapHeight / 2)) * blockWidth };
            }
        }
    };
    ~Program() {};
    Program(const Program&) {};
    const Program& operator=(const Program&) {};

    static Program* instance;

public:

    /**
     * @brief Get the unique instance
     *
     * @return The program instance
     */
    static Program& getInstance();

    /**
     * @brief Refreshes the unique singleton instance
     *
     * Used when the game reloads
     */
    static void refreshInstance();

    // Window data
    int windowWidth = 1280;
    int windowHeight = 720;

    // Game state data
    float timer = 120;
    bool gameWon = false;
    bool gameStarted = false;
    bool startPressed = false;

    // Constant Folders
    std::string RESOURCE_FOLDER = "D:\\Projets Perso\\bomberman\\resources\\";

    // Deltatime
    float deltaTime = 0.0f;
    long lastFrame = 0;

    // Camera
    std::unique_ptr<Camera> camera = std::make_unique<FreeCamera>(glm::vec3(0, 90, 150), glm::vec3(0, 1, 0));

    // Projection
    glm::mat4 projection = glm::perspective(glm::radians(45.0f), (float)windowWidth / windowHeight, 0.1f, 5000.0f);

    // Game Objects
    std::shared_ptr<Player> player;
    std::vector<std::shared_ptr<Box>> boxes;
    std::shared_ptr<Bomb> bomb;
    std::shared_ptr<Monster> dog;
    std::shared_ptr<GraphicObject> blastPowerUp1;
    std::shared_ptr<GraphicObject> blastPowerUp2;
    std::shared_ptr<GraphicObject> speedPowerUp1;
    std::shared_ptr<Explosion> explosion;
    std::shared_ptr<GraphicObject> smallBomb;

    // UI elements
    std::shared_ptr<ImageUI> livesImage;
    std::shared_ptr<ImageUI> gameOverImage;
    std::shared_ptr<ImageUI> winImage;
    std::shared_ptr<ImageUI> tutoImage;
    std::shared_ptr<ImageUI> startImage;

    // Constants
    const float BOMB_HEIGHT = 6.2f;		// 0.0624m		-- > 6.2

    // Shaders
    std::shared_ptr<Shader> basicModelShader;
    std::shared_ptr<Shader> lightShader;

    // Map data
    static const int mapHeight = 13; static const int mapWidth = 13;
    float blockWidth = 20.0f;

    char map[mapWidth][mapHeight] = {
        {'#','#','#','#','#','#','#','#','#','#','#','#','#'},
        {'#','P',' ','b','b','b',' ','b','b','b','b','b','#'},
        {'#',' ','#','b','#','b','#',' ','#','b','#',' ','#'},
        {'#','b','b','b',' ','3',' ','b',' ',' ','b','b','#'},
        {'#','b','#','b','#','b','#','b','#','b','#',' ','#'},
        {'#','b','1',' ',' ',' ',' ','b','2','b',' ','b','#'},
        {'#','b','#',' ','#','b','#',' ','#',' ','#','b','#'},
        {'#','b',' ',' ',' ',' ',' ',' ',' ',' ','M','b','#'},
        {'#','b','#','b','#','b','#','b','#','b','#',' ','#'},
        {'#','b',' ','b',' ','b',' ','b','b','b','b','b','#'},
        {'#',' ','#',' ','#',' ','#','b','#','b','#','b','#'},
        {'#',' ','b','b',' ','b','b',' ',' ',' ','b','E','#'},
        {'#','#','#','#','#','#','#','#','#','#','#','#','#'}
    };

    glm::vec2 mapCoord[mapWidth][mapHeight];

    /**
     * @brief Updates the program deltaTime
     *
     * @param elapsedTime Time between two frames
     */
    void updateDeltaTime(long elapsedTime);


    /**
     * @brief Gets the map array coordinates from the grid position
     *
     * \param coordinates The x and y coordinates in grid coordinates
     * \return The corresponding x and y 2D array positions for the map object
     */
    static glm::vec2 arrayPosFromGridPos(glm::vec2 coordinates) {
        glm::vec2 result;

        if (coordinates.x >= 0) {
            result.x = (float)floor(Program::getInstance().mapWidth / 2) + coordinates.x;
        }
        else {
            result.x = (float)floor(Program::getInstance().mapWidth / 2) - (-1 * coordinates.x);
        }
        if (coordinates.y >= 0) {
            result.y = (float)floor(Program::getInstance().mapHeight / 2) + coordinates.y;
        }
        else {
            result.y = (float)floor(Program::getInstance().mapHeight / 2) + coordinates.y;
        }

        return result;
    }

    /**
     * @brief Gets the grid x and y coordinates from the real world position given
     *
     * \param coordinates The real world x and z coordinates
     * \return The x and y grid positions on the game grid
     */
    static glm::vec2 gridPosFromWorldPos(glm::vec2 coordinates) {
        glm::vec2 result;

        result.x = (int)round(coordinates.x / Program::getInstance().blockWidth);
        result.y = (int)round(coordinates.y / Program::getInstance().blockWidth);

        return result;
    }
};

