#pragma once
/*****************************************************************//**
 * @file   Box.h
 * @brief The Box header file
 *
 * @author theod
 * @date   January 2022
 *********************************************************************/

#include "utils/opengl/GraphicObject.h"

/**
 * @brief The Box class
 */
class Box :
    public GraphicObject
{
public:
    /**
     * @brief The constructor
     *
     * \param modelFilePath The model file path
     * \param shader The shader object
     * \param flip If we need to flip the textures or not
     */
    Box(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip);
};

