#include "Bomb.h"

#include <string>
#include <memory>
#include <iostream>

#include "GL/freeglut.h"

#include "utils/opengl/Shader.h"
#include "Box.h"
#include "Program.h"
#include "ImageUI.h"

/**
 * @brief The constructor
 *
 * \param modelFilePath The model file path
 * \param shader The shader object
 * \param flip If we need to flip the textures or not
 */
Bomb::Bomb(const std::string& modelFilePath, std::shared_ptr<Shader> shader, bool flip) :
	GraphicObject(modelFilePath, shader, flip)
	{}

/**
 * @brief When the bomb explodes
 *
 */
void Bomb::explode() {
	setHidden(true);
	Program::getInstance().explosion->m_drawIt = true;
	Program::getInstance().explosion->m_explosionTime = glutGet(GLUT_ELAPSED_TIME);
	Program::getInstance().smallBomb->setHidden(false);
	m_isOn = false;
	m_detonationTimer = m_detonationTimerMax;

	// Break boxes
	for (std::shared_ptr<Box> box : m_adjacentBoxes) {
		box->setHidden(true);
		glm::vec2 boxArrayPos = Program::arrayPosFromGridPos(box->getTransform().getLevelGridPos());
		Program::getInstance().map[(int)boxArrayPos.x][(int)boxArrayPos.y] = ' ';
	}

	m_adjacentBoxes.clear();

	//give damages
	glm::vec2 bombGridPos = getTransform().getLevelGridPos();
	glm::vec2 bombArrayPos = Program::arrayPosFromGridPos({ bombGridPos.x, bombGridPos.y });
	Program::getInstance().map[(int)bombArrayPos.x][(int)bombArrayPos.y] = ' ';

	glm::vec2 leftTile = { bombGridPos.x - Program::getInstance().bomb->m_power, bombGridPos.y };
	glm::vec2 toptTile = { bombGridPos.x, bombGridPos.y - Program::getInstance().bomb->m_power };
	glm::vec2 rightTile = { bombGridPos.x + Program::getInstance().bomb->m_power, bombGridPos.y };
	glm::vec2 bottomTile = { bombGridPos.x, bombGridPos.y + Program::getInstance().bomb->m_power };
	bool takeDamages = true;

	// Check if a wall is between us and the bomb
	if (Program::getInstance().player->getTransform().getLevelGridPos().x == bombGridPos.x) {
		if (Program::getInstance().player->getTransform().getLevelGridPos().y <= bombGridPos.y && Program::getInstance().player->getTransform().getLevelGridPos().y >= toptTile.y) {
			// above the bomb
			for (int i = Program::getInstance().player->getTransform().getLevelGridPos().y; i <= bombGridPos.y; ++i) {
				glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ toptTile.x ,i });
				takeDamages = true;
				if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
					takeDamages = false;
					break;
				}
			}
		}
		else if (Program::getInstance().player->getTransform().getLevelGridPos().y >= bombGridPos.y && Program::getInstance().player->getTransform().getLevelGridPos().y <= bottomTile.y) {
			// below the bomb
			for (int i = Program::getInstance().player->getTransform().getLevelGridPos().y; i >= bombGridPos.y; --i) {
				glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ bottomTile.x ,i });
				takeDamages = true;
				if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
					takeDamages = false;
					break;
				}
			}
		}
		else {
			takeDamages = false;
		}

		if (takeDamages) {
			Program::getInstance().player->takeDamages(1);
			if (Program::getInstance().player->m_lives == 2) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_2.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 1) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_1.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 0) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_0.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
		}
	}
	else if (Program::getInstance().player->getTransform().getLevelGridPos().y == bombGridPos.y) {
		if (Program::getInstance().player->getTransform().getLevelGridPos().x <= bombGridPos.x && Program::getInstance().player->getTransform().getLevelGridPos().x >= leftTile.x) {
			// above the bomb
			for (int i = Program::getInstance().player->getTransform().getLevelGridPos().x; i <= bombGridPos.x; ++i) {
				glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, leftTile.y });
				takeDamages = true;
				if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
					takeDamages = false;
					break;
				}
			}
		}
		else if (Program::getInstance().player->getTransform().getLevelGridPos().x >= bombGridPos.x && Program::getInstance().player->getTransform().getLevelGridPos().x <= rightTile.x) {
			// above the bomb
			for (int i = Program::getInstance().player->getTransform().getLevelGridPos().x; i >= bombGridPos.x; --i) {
				glm::vec2 arrayTestPos = Program::arrayPosFromGridPos({ i, rightTile.y });
				takeDamages = true;
				if (Program::getInstance().map[(int)arrayTestPos.x][(int)arrayTestPos.y] == '#') {
					takeDamages = false;
					break;
				}
			}
		}
		else {
			takeDamages = false;
		}

		// Now that we know that we need make damages
		if (takeDamages) {
			Program::getInstance().player->takeDamages(1);
			if (Program::getInstance().player->m_lives == 2) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_2.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 1) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_1.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
			else if (Program::getInstance().player->m_lives == 0) {
				Program::getInstance().livesImage = std::make_unique<ImageUI>(Program::getInstance().RESOURCE_FOLDER + "textures\\lives\\Heart_0.png");
				Program::getInstance().livesImage->m_hidden = false;
			}
		}
	}
}
